﻿using Plukit.Base;
using Staxel.Logic;
using Staxel.Tiles;
using Staxel.TileStates;

namespace NimbusFox.NimbusTech.TileStates.DryingRack {
    public class PaintBlockTileStateBuilder : ITileStateBuilder {
        /// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
        public void Dispose() { }
        public void Load() { }
        public string Kind() {
            return KindCode();
        }

        public static string KindCode() {
            return "nimbusfox.nimbusTech.tileState.dryingRack";
        }

        public Entity Instance(Vector3I location, Tile tile, Universe universe) {
            return DryingRackTileStateEntityBuilder.Spawn(location, universe, tile);
        }
    }
}
