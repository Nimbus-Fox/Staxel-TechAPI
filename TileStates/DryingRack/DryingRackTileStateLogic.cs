﻿using System.Linq;
using NimbusFox.NimbusTech.Docks;
using Plukit.Base;
using Staxel.Docks;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Player;
using Staxel.TileStates.Docks;

namespace NimbusFox.NimbusTech.TileStates.DryingRack {
    public class DryingRackTileStateLogic : DockTileStateEntityLogic {

        private EntityId _logicOwner;

        public DryingRackTileStateLogic(Entity entity) : base(entity) {
        }

        protected override void AddSite(DockSiteConfiguration config) {
            _dockSites.Add(new DryingDockSite(Entity, new DockSiteId(Entity.Id, _dockSites.Count), config));
        }

        public override void Interact(Entity user, EntityUniverseFacade facade, ControlState main, ControlState alt) {
            if (user.Logic is PlayerEntityLogic == false) {
                return;
            }

            if (alt.DownClick) {
                var active = user.Inventory.ActiveItem();

                if (active.IsNull()) {
                    foreach (var dock in _dockSites.Cast<DryingDockSite>()) {
                        dock.UnDockResult(user, facade);
                    }
                } else {
                    foreach (var dock in _dockSites.Cast<DryingDockSite>()) {
                        dock.TryDocking(user, active.Item);

                        if (user.Inventory.ActiveItem().IsNull()) {
                            break;
                        }
                    }
                }
            }
        }

        public override bool SuppressInteractVerb() {
            return false;
        }

        public override bool Interactable() {
            return true;
        }
    }
}
