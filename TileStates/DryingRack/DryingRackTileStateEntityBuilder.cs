﻿using Plukit.Base;
using Staxel.Logic;
using Staxel.Tiles;
using Staxel.TileStates.Docks;

namespace NimbusFox.NimbusTech.TileStates.DryingRack {
    public class DryingRackTileStateEntityBuilder : DockTileStateEntityBuilder, IEntityLogicBuilder, IEntityPainterBuilder {
        string IEntityPainterBuilder.Kind => KindCode;
        string IEntityLogicBuilder.Kind => KindCode;

        public new static string KindCode => "nimbusfox.nimbusTech.tileState.dryingRack";

        EntityLogic IEntityLogicBuilder.Instance(Entity entity, bool server) {
            return new DryingRackTileStateLogic(entity);
        }

        public static Entity Spawn(Vector3I position, EntityUniverseFacade universe, Tile tile) {
            var entity = new Entity(universe.AllocateNewEntityId(), false, KindCode, true);

            var blob = BlobAllocator.Blob(true);
            blob.SetString("kind", KindCode);
            blob.FetchBlob("location").SetVector3I(position);
            blob.SetLong("variant", tile.Variant());
            blob.SetString("tile", tile.Configuration.Code);
            blob.FetchBlob("velocity").SetVector3F(Vector3F.Zero);

            entity.Construct(blob, universe);

            universe.AddEntity(entity);

            return entity;
        }
    }
}
