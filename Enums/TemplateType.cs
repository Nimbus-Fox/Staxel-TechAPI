﻿namespace NimbusFox.NimbusTech.Enums {
    public enum TemplateType {
        Tile,
        Item,
        Liquid,
        Gas,
        Recipe,
        Reaction,
        Guide
    }
}