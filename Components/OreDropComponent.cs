﻿using NimbusFox.KitsuneToolBox.Helpers.V1;
using NimbusFox.NimbusTech.Context.Items;
using Plukit.Base;
using Staxel.Items;

namespace NimbusFox.NimbusTech.Components {
    public class OreDropComponent {
        internal OreDropComponent(BaseItemConfiguration item, Blob config) {
            if (HelperFunctions.IsClient()) {
                return;
            }
            
            foreach (var entry in config.KeyValueIteratable) {
                if (entry.Value.Kind == BlobEntryKind.Blob) {
                    NimbusTechContext.OreDropChanceDatabase.AddDropChance(entry.Key, item,
                    new OreChance(entry.Value.Blob()));
                }

                if (entry.Value.Kind == BlobEntryKind.List) {
                    foreach (var listEntry in entry.Value.List()) {
                        if (listEntry.Kind == BlobEntryKind.Blob) {
                            NimbusTechContext.OreDropChanceDatabase.AddDropChance(entry.Key, item,
                                new OreChance(listEntry.Blob()));
                        }
                    }
                }
            }
        }
    }
}