﻿using Plukit.Base;
using Staxel.Items;

namespace NimbusFox.NimbusTech.Components.Builders {
    public class OreDropItemComponentBuilder : IItemComponentBuilder {
        public string Kind() {
            return "oreDropChance";
        }

        public object Instance(BaseItemConfiguration item, Blob config) {
            return new OreDropComponent(item, config);
        }
    }
}