﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using NimbusFox.KitsuneToolBox.BlobDatabase.V1;
using NimbusFox.KitsuneToolBox.Helpers.V1;
using NimbusFox.KitsuneToolBox.Managers.V1;
using NimbusFox.NimbusTech.Context;
using NimbusFox.NimbusTech.Enums;
using Plukit.Base;
using Staxel;
using Staxel.Crafting;

namespace NimbusFox.NimbusTech {
    public static class NimbusTechContext {
        public static OreTemplateDatabase OreTemplateDatabase { get; private set; }
        public static OreDatabase OreDatabase { get; private set; }
        public static OreDropChanceDatabase OreDropChanceDatabase { get; private set; }
        public static GuideDatabase GuideDatabase { get; private set; }
        public static readonly BaseTemplateDatabase BaseTemplateDatabase = new BaseTemplateDatabase();

        public static IReadOnlyList<ReactionDefinition> SunDryReactions { get; private set; } =
            new List<ReactionDefinition>();
        
        internal static DirectoryManager QbCache { get; private set; }
        internal static readonly BlobDatabase CacheDatabase;

        static NimbusTechContext() {
            if (!HelperFunctions.IsServer()) {
                var fs = NimbusTechHook.ToolBox.ConfigDirectory.ObtainFileStream("QBCache.dat", FileMode.OpenOrCreate);
                CacheDatabase = new BlobDatabase(fs, Logger.WriteLine, true);
                
                //ModKeyBindManager.RegisterKeyBind("nimbusfox.nimbusTech.guide", Keys.OemTilde, () => {
                //    GuideDatabase.ShowGuide();
                //});
            }
        }
        
        internal static void InitializeBefore() {
            QbCache = NimbusTechHook.ToolBox.ConfigDirectory.FetchDirectory("QBCache");
            
            OreTemplateDatabase = new OreTemplateDatabase();
            OreDatabase = new OreDatabase();
            OreDropChanceDatabase = new OreDropChanceDatabase();
            
            if (!HelperFunctions.IsClient()) {
            }

            if (!HelperFunctions.IsServer()) {
                GuideDatabase = new GuideDatabase();
            }
        }

        internal static void Initialize() {
            BaseTemplateDatabase.Initialize();
        }

        internal static void InitializeAfter() {
            if (!HelperFunctions.IsClient()) {
            }

            //if (!HelperFunctions.IsServer()) {
            //    var ores = OreDatabase.GetAllOres();
            //    foreach (var template in OreTemplateDatabase.GetAllTemplates(TemplateType.Guide)) {
            //        NimbusTechHook.Log($"Running generator for template \"{template.Code}\"");
            //        foreach (var ore in ores) {
            //            template.Generate(ore);
            //        }
            //    }
            //}

            SunDryReactions = GameContext.ReactionDatabase.GetAllReactionDefinitions().Values
                .Where(x => x.RecipeStep == "sunDry").ToList();
        }

        internal static void ClientInitializeBefore() {
        }

        internal static void ClientInitializeAfter() {
            GuideDatabase.BuildGui();
        }

        internal static void Deinitialize() {
            OreDatabase?.Dispose();
            OreDatabase = null;
            OreTemplateDatabase?.Dispose();
            OreTemplateDatabase = null;
            OreDropChanceDatabase?.Dispose();
            OreDropChanceDatabase = null;

            if (!HelperFunctions.IsClient()) {
                
            }
        }

        internal static void ClientDeinitialize() {
            GuideDatabase?.Dispose();
            GuideDatabase = null;
        }
    }
}