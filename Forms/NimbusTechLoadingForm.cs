﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace NimbusFox.NimbusTech.Forms {
    internal partial class NimbusTechLoadingForm : Form {
        private bool _loop = true;
        public NimbusTechLoadingForm() {
            InitializeComponent();

            txtLog.BackColor = Color.Black;
            txtLog.ForeColor = Color.Lime;

            Show();
        }

        public void WriteLine(string text) {
            if (InvokeRequired) {
                Invoke(new Action<string>(WriteLine), text);
                return;
            }
            txtLog.AppendText(text + Environment.NewLine);
        }

        public void HideForm() {
            if (InvokeRequired) {
                Invoke(new Action(HideForm));
                return;
            }

            Hide();
        }

        public void Clear() {
            txtLog.Clear();
        }

        private void NimbusTechLoadingForm_FormClosed(object sender, FormClosedEventArgs e) {
            _loop = false;
        }
    }
}
