﻿using NimbusFox.KitsuneToolBox.Extensions.V1;
using NimbusFox.KitsuneToolBox.Helpers.V1;
using Plukit.Base;
using Staxel;
using Staxel.Core;
using Staxel.Destruction;
using Staxel.Entities;
using Staxel.Items;
using Staxel.Logic;

namespace NimbusFox.NimbusTech.Patches {
    internal static class DestructionEntityLogicPatches {
        internal static void Initialize() {
            if (HelperFunctions.IsServer()) {
                NimbusTechHook.ToolBox.PatchController.Add(typeof(DestructionEntityLogic), "DropItem", null, null,
                    typeof(DestructionEntityLogicPatches), "DropItem");
            }
        }

        private static void DropItem(DestructionEntityLogic __instance, ItemStack stack, Vector3I position,
            bool useLuck) {
            if (NimbusTechHook.ToolBox.WorldManager.Universe.ReadTile(position, TileAccessFlags.SynchronousWait,
                ChunkFetchKind.LivingWorld, EntityId.NullEntityId, out var tile)) {
                var ore = NimbusTechContext.OreDropChanceDatabase.GetRandomOre(tile.Configuration.Code, position.Y,
                    out var quantity);

                if (ore == Item.NullItem || quantity <= 0) {
                    return;
                }

                var oreStack = new ItemStack(ore, quantity);

                if (useLuck) {
                    var lucky = __instance.GetPrivateFieldValue<bool>("_isLucky");
                    var unlucky = __instance.GetPrivateFieldValue<bool>("_isUnlucky");

                    if (lucky && unlucky) {
                        if (GameContext.RandomSource.Next(100) < Constants.LuckyUnluckyDropChancePercent / 2) {
                            oreStack.Count *= 2;
                        }
                    } else if (lucky && GameContext.RandomSource.Next(100) < Constants.LuckyUnluckyDropChancePercent) {
                        oreStack.Count *= 2;
                    } else if (unlucky && GameContext.RandomSource.Next(100) <
                               Constants.LuckyUnluckyDropChancePercent) {
                        oreStack.Count /= 2;
                    }

                    if (stack.Count < 0) {
                        stack.Count = 0;
                    }

                    if (stack.Count == 0) {
                        return;
                    }
                }

                var parent = __instance.GetPrivateFieldValue<Entity>("_entity");

                ItemEntityBuilder.SpawnDroppedItem(parent, NimbusTechHook.ToolBox.WorldManager.Universe, oreStack,
                    position.ToTileCenterVector3D(), Vector3D.Zero, Vector3D.Zero, SpawnDroppedFlags.AttemptPickup);
            }
        }
    }
}