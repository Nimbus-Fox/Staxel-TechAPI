﻿using System;
using System.Collections.Generic;
using Staxel;

namespace NimbusFox.NimbusTech.UI {
    //internal class Guide : IDisposable {
    //    private readonly List<UIWindow> _activeWindows = new List<UIWindow>();
    //    private UIWindow _guideWindow;
    //    private UIWindow _subGuideWindow;
        
    //    internal Guide() {
    //        _activeWindows.ForEach(x => x?.Dispose());
    //        _activeWindows.Clear();
    //        _guideWindow?.Dispose();
    //        _guideWindow = null;
            
    //        BuildGuideWindow();
    //    }

    //    private void BuildGuideWindow() {
    //        _guideWindow = new UIWindow {CloseOnEscape = false};
    //        _guideWindow.Hide();
    //        _guideWindow.Container.SetBackground("nimbusfox.nimbusTech.background.tablet");
    //        _guideWindow.AdjustToScreen = true;

    //        _guideWindow.OnHide += HideGuide;

    //        _activeWindows.Add(_guideWindow);

    //        var logo = new UILabel(Position.MiddleMiddle);

    //        _guideWindow.Container.AddChild(logo);

    //        logo.SetFont(fontSize: 40);
    //        logo.SetText(ClientContext.LanguageDatabase.GetTranslationString("nimbusfox.nimbusTech.logo.text"));

    //        var spacer = UISpacer.Create(position: Position.MiddleMiddle);
            
    //        _guideWindow.Container.AddChild(spacer);
            
    //        var scanButton = new UIButton(Position.MiddleMiddle);
            
    //        _guideWindow.Container.AddChild(scanButton);
    //        _guideWindow.Container.AddChild(spacer);
            
    //        var scanText = new UILabel(Position.MiddleMiddle);
            
    //        scanText.SetText(ClientContext.LanguageDatabase.GetTranslationString("nimbusfox.nimbusTech.scan.text"));
            
    //        scanButton.AddChild(scanText);

    //        var guideButton = new UIButton(Position.MiddleMiddle);
            
    //        var guideText = new UILabel(Position.MiddleMiddle);
            
    //        guideText.SetText(ClientContext.LanguageDatabase.GetTranslationString("nimbusfox.nimbusTech.guide.text"));
            
    //        guideButton.AddChild(guideText);
            
    //        _guideWindow.Container.AddChild(guideButton);
            
    //        BuildSubGuideWindow(guideButton, spacer);
    //    }

    //    private void BuildSubGuideWindow(UIButton guideButton, UISpacer spacer) {
    //        var guideWindow = new UIWindow {CloseOnEscape = false};
    //        guideWindow.Hide();
    //        guideWindow.AdjustToScreen = true;
    //        guideWindow.Container.SetBackground("nimbusfox.nimbusTech.background.tablet");

    //        guideButton.OnClick += () => {
    //            guideWindow.Show();
    //            _activeWindows.Add(guideWindow);
    //        };

    //        foreach (var chapter in NimbusTechContext.GuideDatabase.Entries) {
    //            guideWindow.Container.AddChild(spacer);
    //            var button = new UIButton(Position.MiddleMiddle);
    //            guideWindow.Container.AddChild(button);
                
    //            var text = new UILabel(Position.MiddleMiddle);
    //            button.AddChild(text);
                
    //            text.SetText(ClientContext.LanguageDatabase.GetTranslationString(chapter.Key + ".chapter"));
    //        }
    //    }
        
    //    internal void ShowGuide() {
    //        ClientContext.WebOverlayRenderer.AcquireInputControl();
    //        _activeWindows.ForEach(x => x.Show());
    //    }

    //    internal void HideGuide() {
    //        ClientContext.WebOverlayRenderer.ReleaseInputControl();
    //        _activeWindows.ForEach(x => x.Hide());
    //    }

    //    public void Dispose() {
    //        _activeWindows.ForEach(x => x.Dispose());
    //        _activeWindows.Clear();
    //        _guideWindow?.Dispose();
    //        _guideWindow = null;
    //    }
    //}
}