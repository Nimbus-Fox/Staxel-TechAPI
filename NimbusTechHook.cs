﻿using System.Threading;
using System.Windows.Forms;
using NimbusFox.KitsuneToolBox;
using NimbusFox.KitsuneToolBox.Helpers.V1;
using NimbusFox.KitsuneToolBox.ToolBox.V1;
using NimbusFox.NimbusTech.Forms;
using NimbusFox.NimbusTech.Patches;
using Plukit.Base;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Modding;
using Staxel.Tiles;

namespace NimbusFox.NimbusTech {
    internal class NimbusTechHook : IModHookV4 {
        public void Dispose() { }

        internal static readonly ToolBox ToolBox;

        static NimbusTechHook() {
            ModHelper.CheckModIsInstalled("NimbusTech", "KitsuneToolBox");
            ToolBox = new ToolBox("NimbusFox", "NimbusTech");
            DestructionEntityLogicPatches.Initialize();
        }

        public void GameContextInitializeInit() {
            NimbusTechContext.Initialize();
        }

        public void GameContextInitializeBefore() {
            NimbusTechContext.InitializeBefore();
        }

        public void GameContextInitializeAfter() {
            NimbusTechContext.InitializeAfter();
        }

        public void GameContextDeinitialize() {
            NimbusTechContext.Deinitialize();
        }

        public void GameContextReloadBefore() { }

        public void GameContextReloadAfter() { }

        public void UniverseUpdateBefore(Universe universe, Timestep step) { }

        public void UniverseUpdateAfter() { }

        public bool CanPlaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            return true;
        }

        public bool CanReplaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            return true;
        }

        public bool CanRemoveTile(Entity entity, Vector3I location, TileAccessFlags accessFlags) {
            return true;
        }

        public void ClientContextInitializeInit() { }

        public void ClientContextInitializeBefore() {
            NimbusTechContext.ClientInitializeBefore();
        }

        public void ClientContextInitializeAfter() {
            NimbusTechContext.ClientInitializeAfter();
        }

        public void ClientContextDeinitialize() {
            NimbusTechContext.ClientDeinitialize();
        }

        public void ClientContextReloadBefore() { }

        public void ClientContextReloadAfter() { }

        public void CleanupOldSession() { }

        public bool CanInteractWithTile(Entity entity, Vector3F location, Tile tile) {
            return true;
        }

        public bool CanInteractWithEntity(Entity entity, Entity lookingAtEntity) {
            return true;
        }

        internal static void Log(string message) {
            Logger.WriteLine($"[NimbusTech] {message}");
        }
    }
}