﻿using System;
using System.Linq;
using Plukit.Base;
using Staxel;
using Staxel.Crafting;
using Staxel.Docks;
using Staxel.Entities;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Player;
using Staxel.TileStates.Docks;

namespace NimbusFox.NimbusTech.Docks {
    public class DryingDockSite : DockSite {
        protected bool CraftFinished;
        protected bool StartTimer;
        protected ReactionDefinition TargetReaction;
        protected TimeSpan ElapsedTime = TimeSpan.Zero;
        private DateTime _lastCheck;

        public DryingDockSite(Entity entity, DockSiteId id, DockSiteConfiguration dockSiteConfig) : base(entity, id,
            dockSiteConfig) {
            _lastCheck = DateTime.Now;
        }

        public override void Update(Timestep timestep, EntityUniverseFacade universe) {
            base.Update(timestep, universe);

            if (StartTimer && !CraftFinished && TargetReaction != null) {
                var calc = DateTime.Now - _lastCheck;
                if (calc.TotalSeconds > 0) {
                    ElapsedTime += calc;
                }

                if (ElapsedTime.TotalSeconds >= TargetReaction.CraftDuration) {
                    CraftFinished = true;

                    var result = TargetReaction.Results.First();

                    EmptyWithoutExploding(universe);

                    AddToDock(null, new ItemStack(GameContext.ItemDatabase.SpawnItem(result.ItemBlob, null),
                        result.Quantity));

                    StartTimer = false;

                    ElapsedTime = TimeSpan.Zero;
                }
            }
            _lastCheck = DateTime.Now;
        }

        public void TryDocking(Entity entity, Item item) {
            if (entity.Logic is PlayerEntityLogic == false) {
                return;
            }

            if (!DockedItem.Stack.IsNull()) {
                return;
            }

            var reaction = NimbusTechContext.SunDryReactions.FirstOrDefault(x =>
                GameContext.ItemDatabase.SpawnItem(x.Reagents[0].ItemBlob, null).GetItemCode() == item.GetItemCode());

            if (reaction?.CraftDuration == null) {
                return;
            }

            entity.Inventory.RemoveItem(new ItemStack(item, 1));

            AddToDock(entity, new ItemStack(item, 1));

            TargetReaction = reaction;

            StartTimer = true;

            ElapsedTime = TimeSpan.Zero;
        }

        public void UnDockResult(Entity entity, EntityUniverseFacade facade) {
            if (entity.Logic is PlayerEntityLogic == false) {
                return;
            }

            if (!CraftFinished) {
                return;
            }

            ItemEntityBuilder.SpawnDroppedItem(entity, facade, DockedItem.Stack, entity.FeetLocation(), Vector3D.Zero, Vector3D.Zero, SpawnDroppedFlags.AttemptPickup);

            EmptyWithoutExploding(facade);

            TargetReaction = null;
        }

        public override void StorePersistenceData(Blob blob) {
            base.StorePersistenceData(blob);

            var blb = blob.FetchBlob("nimbusTech");

            blb.SetLong("elapsed", ElapsedTime.Ticks);

            if (TargetReaction != null) {
                blb.SetString("targetReaction", TargetReaction.Code);
            }

            blb.SetBool("craftFinished", CraftFinished);
        }

        public override void RestoreFromPersistedData(Blob blob) {
            base.RestoreFromPersistedData(blob);

            var blb = blob.FetchBlob("nimbusTech");

            ElapsedTime = new TimeSpan(blb.GetLong("elapsed", ElapsedTime.Ticks));

            if (blb.Contains("targetReaction")) {
                TargetReaction = GameContext.ReactionDatabase.GetReactionDefinition(blb.GetString("targetReaction"));

                CraftFinished = false;
                StartTimer = true;
            }

            CraftFinished = blb.GetBool("craftFinished", CraftFinished);

            if (CraftFinished) {
                StartTimer = false;
            }

            _lastCheck = DateTime.Now;
        }
    }
}
