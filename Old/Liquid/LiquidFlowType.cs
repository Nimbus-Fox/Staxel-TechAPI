﻿namespace NimbusFox.NimbusTech.Liquid {
    public enum LiquidFlowType : byte {
        Straight,
        Corner,
        Collision
    }
}