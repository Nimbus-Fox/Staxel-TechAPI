﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Plukit.Base;
using Staxel.Client;
using Staxel.Core;
using Staxel.Draw;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Rendering;

namespace NimbusFox.NimbusTech.Liquid.Entities.Liquid {
    public class LiquidEntityPainter : EntityPainter {

        /// <inheritdoc />
        protected override void Dispose(bool disposing) { }

        /// <inheritdoc />
        public override void RenderUpdate(Timestep timestep, Entity entity, AvatarController avatarController,
            EntityUniverseFacade facade,
            int updateSteps) {
        }

        /// <inheritdoc />
        public override void ClientUpdate(Timestep timestep, Entity entity, AvatarController avatarController, EntityUniverseFacade facade) { }

        /// <inheritdoc />
        public override void ClientPostUpdate(Timestep timestep, Entity entity, AvatarController avatarController, EntityUniverseFacade facade) { }

        /// <inheritdoc />
        public override void BeforeRender(DeviceContext graphics, Vector3D renderOrigin, Entity entity, AvatarController avatarController,
            Timestep renderTimestep) { }

        /// <inheritdoc />
        public override void Render(DeviceContext graphics, ref Matrix4F matrix, Vector3D renderOrigin, Entity entity,
            AvatarController avatarController, Timestep renderTimestep, RenderMode renderMode) {
            if (entity.Logic is LiquidEntityLogic liquid) {
                if (liquid.LiquidConfiguration != null) {
                    MatrixDrawable target;

                    switch (liquid.FlowType) {
                        default:
                            target = liquid.LiquidConfiguration.StraightFlow[liquid.Phase];
                            break;
                        case LiquidFlowType.Corner:
                            target = liquid.LiquidConfiguration.DiagonalFlow[liquid.Phase];
                            break;
                        case LiquidFlowType.Collision:
                            target = liquid.LiquidConfiguration.CollisionFlow[liquid.Phase];
                            break;
                    }
                    var targetMatrix =
                        Matrix4F.Multiply(
                            Matrix.CreateFromYawPitchRoll(MathHelper.ToRadians(90 * liquid.Rotation), 0, 0).ToMatrix4F()
                                .Translate((liquid.TileLocation.ToVector3F() - renderOrigin.ToVector3F()) + new Vector3F(0.5f, 0, 0.5f)), matrix);

                    target.Render(graphics, ref targetMatrix);
                }
            }
        }

        /// <inheritdoc />
        public override void StartEmote(Entity entity, Timestep renderTimestep, EmoteConfiguration emote) { }
    }
}
