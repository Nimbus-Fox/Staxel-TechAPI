﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.KitsuneCore.V1;
using Plukit.Base;
using Staxel.Collections;
using Staxel.Core;
using Staxel.Items;
using Staxel.Logic;
using Constants = Staxel.Core.Constants;

namespace NimbusFox.NimbusTech.Liquid.Entities.Liquid {
    public class LiquidEntityLogic : EntityLogic {
        protected Entity _entity { get; }
        protected Blob _constructor { get; private set; }
        public Vector3I TileLocation { get; private set; }
        public LiquidConfiguration LiquidConfiguration { get; private set; }
        public byte Phase { get; private set; }
        public LiquidFlowType FlowType { get; private set; } = LiquidFlowType.Straight;
        public bool IsSource { get; private set; }
        private Entity _parent;
        private DateTime _lastUpdate = DateTime.Now;
        private bool _remove;
        private bool _constructed;
        public byte Rotation { get; private set; }
        protected bool NeedStorage { get; private set; }
        private bool _updateFlow = false;
        private long _newParent = -1;

        private static readonly List<(byte rotation, Vector3I location)> PlusPositions = new List<(byte rotation, Vector3I location)> {
            (0, new Vector3I(1, 0, 0)),
            (1, new Vector3I(0, 0, -1)),
            (2, new Vector3I(-1, 0, 0)),
            (3, new Vector3I(0, 0, 1))
        };

        public LiquidEntityLogic(Entity entity) {
            _entity = entity;
        }

        /// <inheritdoc />
        public override void PreUpdate(Timestep timestep, EntityUniverseFacade entityUniverseFacade) {
            if (_newParent != -1) {
                if (entityUniverseFacade.TryGetEntity(_newParent, out var parent)) {
                    _parent = parent;
                } else {
                    _parent = null;
                    if (_constructor.Contains("parent")) {
                        _constructor.Delete("parent");
                    }
                }

                _newParent = -1;
            }
            if (!_updateFlow) {
                return;
            }

            _updateFlow = false;
            var above = false;
            entityUniverseFacade.ForAllEntitiesInRange((TileLocation + new Vector3I(0, 1, 0)).ToVector3D(), 2, entity => {
                if (entity.Logic is LiquidEntityLogic logic) {
                    if (logic.TileLocation == TileLocation + new Vector3I(0, 1, 0)) {
                        above = true;
                        Phase = 0;
                        FlowType = LiquidFlowType.Collision;
                    }
                }
            });

            if (!above) {
                var max = (byte)0;
                var count = (byte)0;

                entityUniverseFacade.ForAllEntitiesInRange(TileLocation.ToVector3D(), 1, entity => {
                    if (entity.Logic is LiquidEntityLogic logic) {
                        foreach (var item in PlusPositions) {
                            if (item.location == logic.TileLocation) {
                                if (max > logic.Phase) {
                                    max = logic.Phase;
                                    count = 1;
                                    Rotation = item.rotation;
                                } else if (max == logic.Phase) {
                                    count++;
                                    Rotation = item.rotation;
                                }

                                return;
                            }
                        }
                    }
                });
                if (count == 2) {
                    FlowType = LiquidFlowType.Corner;
                } else if (count > 2) {
                    FlowType = LiquidFlowType.Collision;
                } else {
                    FlowType = LiquidFlowType.Straight;
                }
                Phase = max;
            }

            NeedStore();
        }

        /// <inheritdoc />
        public override void Update(Timestep timestep, EntityUniverseFacade entityUniverseFacade) {
            if (!_constructed) {
                _lastUpdate = DateTime.Now;
                return;
            }
            if ((DateTime.Now - _lastUpdate).TotalSeconds >= LiquidConfiguration.Delay) {
                _lastUpdate = DateTime.Now;
                if (_parent?.Removed == false) {
                    if (!SpreadDown(entityUniverseFacade)) {
                        var next = NextStage();
                        if (next == 255) {
                            return;
                        }
                        Spread(entityUniverseFacade);
                    }

                    if (_parent?.Logic is LiquidEntityLogic logic) {
                        if (logic.Phase == Phase) {
                            var next = NextStage();
                            if (next == 255) {
                                _remove = true;
                            } else {
                                Phase = next;
                                NeedStore();
                            }
                        }
                    }
                } else {
                    var next = NextStage();

                    if (next == 255) {
                        _remove = true;
                    } else {
                        Phase = next;
                        NeedStore();
                    }
                }
            }
        }

        /// <inheritdoc />
        public override void PostUpdate(Timestep timestep, EntityUniverseFacade entityUniverseFacade) {
            if (_remove) {
                _entity.SetRemoved();
                entityUniverseFacade.RemoveEntity(_entity.Id);
                return;
            }
            if (entityUniverseFacade.ReadTile(TileLocation, TileAccessFlags.SynchronousWait, ChunkFetchKind.LivingWorld,
                _entity.Id, out var tile)) {
                if (tile.Configuration.Code != Constants.SkyCode) {
                    _entity.SetRemoved();
                    entityUniverseFacade.RemoveEntity(_entity.Id);
                }
            }
        }

        /// <inheritdoc />
        public override void Store() {
            if (NeedStorage) {
                var configBlob = _entity.Blob.FetchBlob("nimbusTech");
                configBlob.SetString("liquid", LiquidConfiguration.Code);
                configBlob.FetchBlob("location").SetVector3I(TileLocation);
                configBlob.SetLong("phase", Phase);
                configBlob.SetLong("flowType", (byte)FlowType);
                configBlob.SetBool("source", IsSource);
                configBlob.SetLong("rotation", Rotation);
                NeedStorage = false;
            }
        }

        /// <inheritdoc />
        public override void Restore() {
            var configBlob = _entity.Blob.FetchBlob("nimbusTech");
            if (configBlob.Contains("liquid")) {
                if (NimbusTechAPI.LiquidDatabase.TryGetLiquid(configBlob.GetString("liquid"), out var config)) {
                    LiquidConfiguration = config;
                }
            }

            if (configBlob.Contains("location")) {
                TileLocation = configBlob.FetchBlob("location").GetVector3I();
            }

            if (configBlob.Contains("phase")) {
                Phase = configBlob.FetchByte("phase");
            }

            if (configBlob.Contains("flowType")) {
                FlowType = (LiquidFlowType)configBlob.FetchByte("flowType");
            }

            if (configBlob.Contains("source")) {
                IsSource = configBlob.GetBool("source");
            }

            if (configBlob.Contains("rotation")) {
                Rotation = configBlob.FetchByte("rotation");
            }
        }

        /// <inheritdoc />
        public override void Construct(Blob arguments, EntityUniverseFacade entityUniverseFacade) {
            _constructor = arguments.Clone();

            _entity.Physics.MakePhysicsless();
            TileLocation = arguments.FetchBlob("location").GetVector3I();
            _entity.Physics.ForcedPosition(TileLocation.ToVector3D());

            if (NimbusTechAPI.LiquidDatabase.TryGetLiquid(arguments.GetString("liquid"), out var config)) {
                LiquidConfiguration = config;
            }

            Phase = arguments.FetchByte("phase");

            IsSource = arguments.GetBool("source", false);

            if (IsSource) {
                MakeSource();
            } else {
                AssignParent(arguments.GetLong("parent", 0));
            }

            FlowType = (LiquidFlowType)arguments.GetLong("flowType", 0);

            Rotation = arguments.FetchByte("rotation");

            NeedStore();
            _constructed = true;
        }

        /// <inheritdoc />
        public override void Bind() { }

        /// <inheritdoc />
        public override bool Interactable() {
            return false;
        }

        /// <inheritdoc />
        public override void Interact(Entity entity, EntityUniverseFacade facade, ControlState main, ControlState alt) { }

        /// <inheritdoc />
        public override bool CanChangeActiveItem() {
            return false;
        }

        /// <inheritdoc />
        public override Heading Heading() {
            return default;
        }

        /// <inheritdoc />
        public override bool IsPersistent() {
            return true;
        }

        /// <inheritdoc />
        public override void StorePersistenceData(Blob data) {
            _constructor.SetLong("phase", Phase);
            if (_parent != null) {
                _constructor.SetLong("parent", _parent.Id.Id);
            }
            _constructor.SetLong("flowType", (byte)FlowType);
            _constructor.SetBool("source", IsSource);
            _constructor.SetLong("rotation", Rotation);

            data.FetchBlob("constructor").MergeFrom(_constructor, true);
        }

        /// <inheritdoc />
        public override void RestoreFromPersistedData(Blob data, EntityUniverseFacade facade) {
            if (data.Contains("constructor")) {
                Construct(data.FetchBlob("constructor"), facade);
            }
        }

        /// <inheritdoc />
        public override bool IsCollidable() {
            return false;
        }

        protected void NeedStore() {
            NeedStorage = true;
        }

        public void MakeSource() {
            IsSource = true;
            Phase = 0;
            FlowType = LiquidFlowType.Collision;
            _parent = _entity;
            NeedStore();
        }

        private void UpdateFlow() {
            _updateFlow = true;
        }

        private void AssignParent(long id) {
            _newParent = id;
        }

        protected bool SpreadDown(EntityUniverseFacade entityUniverseFacade) {
            if (entityUniverseFacade.ReadTile(TileLocation - new Vector3I(0, 1, 0), TileAccessFlags.SynchronousWait,
                ChunkFetchKind.LivingWorld, EntityId.NullEntityId, out var tile)) {
                if (tile.Configuration.Code != Constants.SkyCode) {
                    return false;
                }
            }

            var liquidBelow = false;

            entityUniverseFacade.ForAllEntitiesInRange((TileLocation - new Vector3I(0, 1, 0)).ToVector3D(), 2,
                entity => {
                    if (entity.Logic is LiquidEntityLogic liquid) {
                        if (liquid.TileLocation == TileLocation - new Vector3I(0, 1, 0)) {
                            liquidBelow = !liquid._entity.Removed;
                            if (!liquid.IsSource) {
                                liquid.AssignParent(_entity.Id.Id);
                                liquid.UpdateFlow();
                            }
                        }
                    }
                });

            if (!liquidBelow) {
                LiquidEntityBuilder.Spawn(TileLocation - new Vector3I(0, 1, 0), entityUniverseFacade,
                    LiquidConfiguration.Code, false, _entity.Id);
            }

            return true;
        }

        protected void Spread(EntityUniverseFacade entityUniverseFacade) {
            foreach (var location in PlusPositions) {
                if (entityUniverseFacade.ReadTile(TileLocation + location.location, TileAccessFlags.SynchronousWait,
                    ChunkFetchKind.LivingWorld, EntityId.NullEntityId, out var tile)) {
                    if (tile.Configuration.Code != Constants.SkyCode) {
                        continue;
                    }
                }

                var liquidExists = false;

                entityUniverseFacade.ForAllEntitiesInRange((TileLocation + location.location).ToVector3D(), 2, entity => {
                    if (entity.Logic is LiquidEntityLogic liquid) {
                        if (liquid.TileLocation == TileLocation + location.location) {
                            liquidExists = !liquid._entity.Removed;
                            if (!liquid.IsSource) {
                                if (liquid._entity.Id == _parent.Id || liquid._parent?.Removed == true || liquid._parent?.Id == _entity.Id || !liquid._constructed) {
                                    return;
                                }
                                if (liquid.Phase > NextStage()) {
                                    liquid.AssignParent(_entity.Id.Id);
                                    liquid.UpdateFlow();
                                }
                            }
                        }
                    }
                });

                if (!liquidExists) {
                    LiquidEntityBuilder.Spawn(TileLocation + location.location, entityUniverseFacade, LiquidConfiguration.Code,
                        false, _entity.Id, LiquidFlowType.Straight, location.rotation, NextStage());
                }
            }
        }

        public byte NextStage(bool increase = false) {
            var next = Phase;

            if (increase && next != 0) {
                next--;
            } else if (!increase && next != 255) {
                next++;
            }

            if (FlowType == LiquidFlowType.Straight) {
                if (next > LiquidConfiguration.StraightFlow.Length - 1) {
                    return 255;
                }
            } else if (FlowType == LiquidFlowType.Corner) {
                if (next > LiquidConfiguration.DiagonalFlow.Length - 1) {
                    return 255;
                }
            } else if (FlowType == LiquidFlowType.Collision) {
                if (next > LiquidConfiguration.CollisionFlow.Length - 1) {
                    return 255;
                }
            }
            return next;
        }
    }
}
