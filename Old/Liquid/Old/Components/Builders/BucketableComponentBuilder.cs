﻿using Plukit.Base;
using Staxel.Core;
using Staxel.Items;

namespace NimbusFox.NimbusTech.Liquid.Components.Builders {
    public class BucketableComponentBuilder : IComponentBuilder, IItemComponentBuilder {
        public string Kind() {
            return "bucketable";
        }

        public object Instance(BaseItemConfiguration item, Blob config) {
            return new BucketableComponent(config);
        }

        public object Instance(Blob config) {
            return new BucketableComponent(config);
        }
    }
}
