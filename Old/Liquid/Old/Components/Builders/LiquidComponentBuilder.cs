﻿using Plukit.Base;
using Staxel.Core;

namespace NimbusFox.NimbusTech.Liquid.Components.Builders {
    public class LiquidComponentBuilder : IComponentBuilder {
        public string Kind() {
            return "liquid";
        }

        public object Instance(Blob config) {
            return new LiquidComponent(config);
        }
    }
}
