﻿using System;
using Plukit.Base;

namespace NimbusFox.NimbusTech.Liquid.Components {
    public class LiquidComponent {
        public bool IsSource { get; }
        public double Speed { get; }
        public bool IsHot { get; }
        public PunishmentAmounts Punishment { get; }

        public string CenterTile { get; }
        public string SourceTile { get; }
        public Tiles Straight { get; }

        public class PunishmentAmounts {
            public int Max { get; }
            public int Min { get; }

            public PunishmentAmounts(Blob config) {
                Max = Convert.ToInt32(config.GetLong("max", 25));
                Min = Convert.ToInt32(config.GetLong("min", 1));
            }
        }

        public class Tiles {
            public string H1 { get; }
            public string H2 { get; }
            public string H4 { get; }
            public string H6 { get; }
            public string H8 { get; }
            public string H10 { get; }
            public string H12 { get; }
            public string H14 { get; }

            public Tiles(Blob config) {
                H1 = config.GetString("0");
                H2 = config.GetString("1");
                H4 = config.GetString("2");
                H6 = config.GetString("3");
                H8 = config.GetString("4");
                H10 = config.GetString("5");
                H12 = config.GetString("6");
                H14 = config.GetString("7");
            }
        }

        public LiquidComponent(Blob config) {
            IsSource = config.GetBool("source", false);
            Speed = config.GetDouble("speed", 1.0);
            IsHot = config.GetBool("hot", false);
            Punishment = new PunishmentAmounts(config.FetchBlob("punishmentCost"));
            CenterTile = config.GetString("centerTile");
            SourceTile = config.GetString("sourceTile");
            Straight = new Tiles(config.GetBlob("straightTiles"));
        }
    }
}
