﻿using System;
using Plukit.Base;
using Staxel.Items.ItemComponents;

namespace NimbusFox.NimbusTech.Liquid.Components {
    public class BucketableComponent : IToolable {
        public int ContentAmount { get; }

        public BucketableComponent(Blob config) {
            ContentAmount = Convert.ToInt32(config.GetLong("contentAmount", 1000));
        }

        public string ToolCode() {
            return "nimbusfox.nimbustech.verb.bucketable";
        }

        public string VerbTranslationCode() {
            return "nimbusfox.nimbustech.verb.needBucket";
        }

        /// <inheritdoc />
        public int StrengthRequired() {
            return 0;
        }
    }
}
