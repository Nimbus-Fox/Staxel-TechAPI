﻿using Plukit.Base;
using Staxel.Logic;
using Staxel.Tiles;
using Staxel.TileStates;

namespace NimbusFox.NimbusTech.Liquid.BaseLiquid.TileState {
    public class BaseLiquidTileStateBuilder : ITileStateBuilder {
        public void Dispose() { }
        public void Load() { }
        public string Kind() {
            return "nimbusfox.nimbustech.tileState.baseLiquid";
        }

        public Entity Instance(Vector3I location, Tile tile, Universe universe) {
            return BaseLiquidTileStateEntityBuilder.Spawn(universe, tile, location);
        }
    }
}
