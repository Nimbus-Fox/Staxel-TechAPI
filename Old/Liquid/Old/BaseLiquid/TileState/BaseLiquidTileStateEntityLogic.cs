﻿using System.Linq;
using NimbusFox.NimbusTech.Liquid.BaseLiquid.TileEntity;
using Plukit.Base;
using Staxel.Logic;
using Staxel.TileStates;

namespace NimbusFox.NimbusTech.Liquid.BaseLiquid.TileState {
    public class BaseLiquidTileStateEntityLogic : TileStateEntityLogic {

        private EntityId _logicOwner = EntityId.NullEntityId;

        public BaseLiquidTileStateEntityLogic(Entity entity) : base(entity) { }
        public override void PreUpdate(Timestep timestep, EntityUniverseFacade entityUniverseFacade) {

            if (_logicOwner == EntityId.NullEntityId) {

                var entities = new Lyst<Entity>();

                entityUniverseFacade.FindAllEntitiesInRange(entities, Location.ToVector3D(), 1F, entity => {
                    if (entity.Removed) {
                        return false;
                    }

                    if (entity.Logic is BaseLiquidTileEntityLogic logic) {
                        return true;
                    }

                    return false;
                });

                var tileEntity = entities.FirstOrDefault();


                if (tileEntity != default(Entity)) {
                    _logicOwner = tileEntity.Id;
                } else {
                    _logicOwner = BaseLiquidTileEntityBuilder.Spawn(Location, entityUniverseFacade).Id;
                }
            }
        }
        public override void Update(Timestep timestep, EntityUniverseFacade entityUniverseFacade) { }
        public override void PostUpdate(Timestep timestep, EntityUniverseFacade entityUniverseFacade) { }

        public override void Construct(Blob arguments, EntityUniverseFacade entityUniverseFacade) {
            Location = arguments.FetchBlob("location").GetVector3I();
        }
        public override void Bind() { }
        public override bool Interactable() {
            return false;
        }

        public override void Interact(Entity entity, EntityUniverseFacade facade, ControlState main, ControlState alt) { }
        public override bool CanChangeActiveItem() {
            return false;
        }

        public override bool IsPersistent() {
            return false;
        }

        public override bool IsLingering() {
            return false;
        }

        public override void KeepAlive() { }
        public override void BeingLookedAt(Entity entity) { }
        public override bool IsBeingLookedAt() {
            return false;
        }
    }
}
