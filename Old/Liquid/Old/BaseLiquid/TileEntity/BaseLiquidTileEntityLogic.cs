﻿using System;
using NimbusFox.KitsuneCore.V1;
using NimbusFox.NimbusTech.Liquid.Components;
using Plukit.Base;
using Staxel;
using Staxel.Core;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Tiles;
using Constants = Staxel.Core.Constants;

namespace NimbusFox.NimbusTech.Liquid.BaseLiquid.TileEntity {
    public class BaseLiquidTileEntityLogic : EntityLogic {
        protected bool NeedStore { get; private set; } = true;
        protected Entity Entity { get; }
        protected Vector3I Location { get; private set; }
        private Blob _constructionBlob;
        protected TileConfiguration TileConfig { get; private set; }
        private DateTime _lastUpdate = DateTime.Now;
        protected LiquidComponent LiquidComponent { get; private set; }

        public BaseLiquidTileEntityLogic(Entity entity) {
            Entity = entity;
        }

        public override void PreUpdate(Timestep timestep, EntityUniverseFacade entityUniverseFacade) {
            if (entityUniverseFacade.ReadTile(Location, TileAccessFlags.SynchronousWait, ChunkFetchKind.LivingWorld, EntityId.NullEntityId, out var tile)) {
                TileConfig = tile.Configuration;
            }
        }

        public override void Update(Timestep timestep, EntityUniverseFacade entityUniverseFacade) {
            if ((DateTime.Now - _lastUpdate).TotalSeconds >= LiquidComponent.Speed) {
                _lastUpdate = DateTime.Now;
                var newTileLocation = new Vector3I(Location.X, Location.Y - 1, Location.Z);
                if (entityUniverseFacade.ReadTile(newTileLocation, TileAccessFlags.SynchronousWait, ChunkFetchKind.LivingWorld, EntityId.NullEntityId, out var tile)) {
                    if (tile.Configuration.Code == Constants.SkyCode || IsLiquidTile(tile.Configuration.Code)
                        && tile.Configuration.Code != LiquidComponent.SourceTile && tile.Configuration.Code != LiquidComponent.CenterTile) {
                        entityUniverseFacade.PlaceTile(Entity, newTileLocation,
                            Helpers.MakeTile(LiquidComponent.CenterTile), TileAccessFlags.SynchronousWait, ChunkFetchKind.LivingWorld, EntityId.NullEntityId);
                        if (entityUniverseFacade.TryFetchTileStateEntityLogic(newTileLocation,
                            TileAccessFlags.SynchronousWait, ChunkFetchKind.LivingWorld, EntityId.NullEntityId, out var logic)) {
                            logic.Update(timestep, entityUniverseFacade);
                        }
                    }
                    if (IsLiquidTile(TileConfig.Code)) {
                        var hasSource = LiquidComponent.IsSource;
                        for (var x = -1; x <= 1; x++) {
                            for (var z = -1; z <= 1; z++) {
                                for (var y = 0; y <= 1; y++) {
                                    var pos = new Vector3I(Location.X + x, Location.Y + y, Location.Z + z);
                                    if (pos == Location) {
                                        continue;
                                    }

                                    if (y == 1 && (x != 0 || z != 0)) {
                                        continue;
                                    }

                                    if (entityUniverseFacade.ReadTile(pos, TileAccessFlags.SynchronousWait, ChunkFetchKind.LivingWorld, EntityId.NullEntityId, out var sourceTile)) {
                                        if (y == 1 && x == 0 && z == 0) {
                                            if (IsLiquidTile(sourceTile.Configuration.Code)) {
                                                hasSource = true;
                                            }
                                        }

                                        if (IsLiquidTile(sourceTile.Configuration.Code) &&
                                            (NextTile(true) == sourceTile.Configuration.Code) ||
                                            sourceTile.Configuration.Code == LiquidComponent.CenterTile ||
                                            sourceTile.Configuration.Code == LiquidComponent.SourceTile) {
                                            hasSource = true;
                                        }
                                    }
                                }
                            }
                        }

                        if (!hasSource) {
                            var next = NextTile(false);
                            if (TileConfig.Code != next) {
                                if (next == Constants.SkyCode) {
                                    entityUniverseFacade.RemoveTile(Entity, Location, TileAccessFlags.SynchronousWait, ChunkFetchKind.LivingWorld, EntityId.NullEntityId);
                                    goto doneRendering;
                                }
                                entityUniverseFacade.PlaceTile(Entity, Location,
                                    GameContext.TileDatabase.GetTileConfiguration(next).MakeTile(tile.Variant()),
                                    TileAccessFlags.SynchronousWait, ChunkFetchKind.LivingWorld, EntityId.NullEntityId);
                            }
                        } else {
                            if (entityUniverseFacade.ReadTile(new Vector3I(Location.X, Location.Y - 1, Location.Z),
                                TileAccessFlags.SynchronousWait, ChunkFetchKind.LivingWorld, EntityId.NullEntityId, out var belowTile)) {
                                if (IsLiquidTile(belowTile.Configuration.Code)) {
                                    goto doneRendering;
                                }

                                var straight = new[] {
                                    new Vector3I(Location.X + 1, Location.Y, Location.Z),
                                    new Vector3I(Location.X - 1, Location.Y, Location.Z),
                                    new Vector3I(Location.X, Location.Y, Location.Z + 1),
                                    new Vector3I(Location.X, Location.Y, Location.Z - 1)
                                };

                                foreach (var item in straight) {
                                    if (entityUniverseFacade.ReadTile(item, TileAccessFlags.SynchronousWait, ChunkFetchKind.LivingWorld, EntityId.NullEntityId,
                                        out var targetTile)) {
                                        if (targetTile.Configuration.Code == Constants.SkyCode) {
                                            entityUniverseFacade.PlaceTile(Entity, item, Helpers.MakeTile(NextTile(false)), TileAccessFlags.SynchronousWait, ChunkFetchKind.LivingWorld, EntityId.NullEntityId);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    doneRendering:
                    if (LiquidComponent.IsHot) {
                        entityUniverseFacade.ForAllEntitiesInRange(Location.ToVector3D(), 2.5F, entity => {
                            if (entity.Removed) {
                                return;
                            }

                            if (entity.Inventory == null) {
                                return;
                            }

                            var money = entity.Inventory.GetMoney();

                            if (money > 0) {
                                var toTake = GameContext.RandomSource.Next(LiquidComponent.Punishment.Min,
                                    LiquidComponent.Punishment.Max);

                                if (money <= toTake) {
                                    entity.Inventory.ResetMoney(0);
                                } else {
                                    entity.Inventory.ResetMoney(money - toTake);
                                }
                            }
                        });
                    }
                }
            }
        }

        public override void PostUpdate(Timestep timestep, EntityUniverseFacade entityUniverseFacade) {
            if (!IsLiquidTile(TileConfig.Code)) {
                entityUniverseFacade.RemoveEntity(Entity.Id);
            }
        }

        public bool IsLiquidTile(string liquid) {
            return liquid == LiquidComponent.Straight.H14 || liquid == LiquidComponent.Straight.H12 ||
                   liquid == LiquidComponent.Straight.H10
                   || liquid == LiquidComponent.Straight.H8 || liquid == LiquidComponent.Straight.H6 ||
                   liquid == LiquidComponent.Straight.H4
                   || liquid == LiquidComponent.Straight.H2 || liquid == LiquidComponent.Straight.H1
                   || liquid == LiquidComponent.SourceTile || liquid == LiquidComponent.CenterTile;
        }

        public string NextTile(bool increase) {
            if (TileConfig.Code == LiquidComponent.Straight.H14) {
                return increase ? LiquidComponent.CenterTile : LiquidComponent.Straight.H12;
            }

            if (TileConfig.Code == LiquidComponent.Straight.H12) {
                return increase ? LiquidComponent.Straight.H14 : LiquidComponent.Straight.H10;
            }

            if (TileConfig.Code == LiquidComponent.Straight.H10) {
                return increase ? LiquidComponent.Straight.H12 : LiquidComponent.Straight.H8;
            }

            if (TileConfig.Code == LiquidComponent.Straight.H8) {
                return increase ? LiquidComponent.Straight.H10 : LiquidComponent.Straight.H6;
            }

            if (TileConfig.Code == LiquidComponent.Straight.H6) {
                return increase ? LiquidComponent.Straight.H8 : LiquidComponent.Straight.H4;
            }

            if (TileConfig.Code == LiquidComponent.Straight.H4) {
                return increase ? LiquidComponent.Straight.H6 : LiquidComponent.Straight.H2;
            }

            if (TileConfig.Code == LiquidComponent.Straight.H2) {
                return increase ? LiquidComponent.Straight.H4 : LiquidComponent.Straight.H1;
            }

            if (TileConfig.Code == LiquidComponent.Straight.H1) {
                return increase ? LiquidComponent.Straight.H2 : Constants.SkyCode;
            }

            return increase ? LiquidComponent.CenterTile : LiquidComponent.Straight.H14;
        }

        public override void Store() {
            if (NeedStore) {
                NeedStore = false;
                StorePersistenceData(Entity.Blob);
            }
        }

        public override void Restore() {
            Restore(Entity.Blob);
        }

        protected void Restore(Blob blob) {

        }

        public override void Construct(Blob arguments, EntityUniverseFacade entityUniverseFacade) {
            _constructionBlob = BlobAllocator.Blob(false);
            _constructionBlob.AssignFrom(arguments);
            Location = arguments.FetchBlob("location").GetVector3I();
            Entity.Physics.ForcedPosition(Location.ToVector3D());
            if (entityUniverseFacade.ReadTile(Location, TileAccessFlags.SynchronousWait, ChunkFetchKind.LivingWorld, EntityId.NullEntityId, out var tile)) {
                LiquidComponent = tile.Configuration.Components.Get<LiquidComponent>();
            }
            NeedsStore();
        }
        public override void Bind() { }
        public override bool Interactable() {
            return false;
        }

        public override void Interact(Entity entity, EntityUniverseFacade facade, ControlState main, ControlState alt) { }
        public override bool CanChangeActiveItem() {
            return false;
        }

        public override Heading Heading() {
            return new Heading();
        }

        public override bool IsPersistent() {
            return true;
        }

        public override void StorePersistenceData(Blob data) {
            if (data != Entity.Blob) {
                data.FetchBlob("constructors").AssignFrom(_constructionBlob);
            }
        }

        public override void RestoreFromPersistedData(Blob data, EntityUniverseFacade facade) {
            if (data.Contains("constructors")) {
                Construct(data.FetchBlob("constructors"), facade);
            }

            Restore(data);
        }

        public override bool IsCollidable() {
            return false;
        }

        public void NeedsStore() {
            NeedStore = true;
        }
    }
}
