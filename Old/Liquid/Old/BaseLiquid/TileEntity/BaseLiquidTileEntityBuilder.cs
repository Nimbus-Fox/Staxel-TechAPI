﻿using Plukit.Base;
using Staxel.Logic;
using Staxel.TileStates;

namespace NimbusFox.NimbusTech.Liquid.BaseLiquid.TileEntity {
    public class BaseLiquidTileEntityBuilder : IEntityLogicBuilder2, IEntityLogicBuilder, IEntityPainterBuilder {
        public static string KindCode => "nimbusfox.nimbustech.tileEntity.baseLiquid";
        public string Kind => KindCode;

        public EntityLogic Instance(Entity entity, bool server) {
            return new BaseLiquidTileEntityLogic(entity);
        }

        public EntityPainter Instance() {
            return new HiddenEntityPainter();
        }

        public void Load() { }

        public bool IsTileStateEntityKind() {
            return false;
        }

        public static Entity Spawn(Vector3I position, EntityUniverseFacade universe, string kind = "nimbusfox.nimbustech.tileEntity.baseLiquid") {
            var entity = new Entity(universe.AllocateNewEntityId(), false, kind, true);

            var blob = BlobAllocator.Blob(true);
            blob.SetString("kind", kind);
            blob.FetchBlob("position").SetVector3D(position.ToTileCenterVector3D());
            blob.FetchBlob("location").SetVector3I(position);

            entity.Construct(blob, universe);

            universe.AddEntity(entity);

            return entity;
        }
    }
}
