﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.KitsuneCore.V1;
using NimbusFox.KitsuneCore.V1.Animation.Interfaces;
using Plukit.Base;
using Staxel;
using Staxel.Items;

namespace NimbusFox.NimbusTech.Liquid.Items.LiquidPlacer {
    public class LiquidPlacerItemBuilder : IItemBuilderHelper {
        /// <inheritdoc />
        public void Dispose() { }

        /// <inheritdoc />
        public void Load() {
            Renderer = new ItemRenderer();
        }

        /// <inheritdoc />
        public Item Build(Blob blob, ItemConfiguration configuration, Item spare) {
            var item = new LiquidPlacerItem(this, configuration);

            item.Restore(configuration, blob);

            return item;
        }

        /// <inheritdoc />
        public string Kind() {
            return "nimbusfox.nimbustech.item.liquid";
        }

        public static Item Spawn(LiquidConfiguration config) {
            if (Helpers.TryGetItemBuilder("nimbusfox.nimbustech.item.liquid", out var builder)) {
                if (builder is LiquidPlacerItemBuilder liquidPlacerBuilder) {
                    return liquidPlacerBuilder.Build(BlobAllocator.Blob(true), config, Item.NullItem);
                }
            }

            return Item.NullItem;
        }

        /// <inheritdoc />
        public ItemRenderer Renderer { get; private set; }
    }
}
