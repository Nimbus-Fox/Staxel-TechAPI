﻿using NimbusFox.NimbusTech.Liquid.Entities.Liquid;
using Plukit.Base;
using Staxel.Client;
using Staxel.Collections;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Tiles;

namespace NimbusFox.NimbusTech.Liquid.Items.LiquidPlacer {
    public class LiquidPlacerItem : Item {

        private readonly LiquidPlacerItemBuilder _builder;

        public LiquidPlacerItem(LiquidPlacerItemBuilder builder, ItemConfiguration config) : base(builder.Kind()) {
            Configuration = config;
            _builder = builder;
        }

        /// <inheritdoc />
        public override void Update(Entity entity, Timestep step, EntityUniverseFacade entityUniverseFacade) { }

        /// <inheritdoc />
        public override void Control(Entity entity, EntityUniverseFacade facade, ControlState main, ControlState alt) {
            if (main.UpClick) {
                if (entity.PlayerEntityLogic.LookingAtTile(out _, out var target)) {
                    var canPlace = true;
                    facade.ForAllEntitiesInRange(target.ToVector3D(), 2, ent => {
                        if (ent.Logic is LiquidEntityLogic liquid) {
                            if (liquid.TileLocation == target) {
                                canPlace = false;
                                if (!liquid.IsSource) {
                                    liquid.MakeSource();
                                }
                            }
                        }
                    });

                    if (canPlace) {
                        LiquidEntityBuilder.Spawn(target, facade, Configuration.Code, true);
                    }
                }
            }
        }

        /// <inheritdoc />
        public override bool Same(Item item) {
            if (item == NullItem) {
                return false;
            }

            if (item.GetItemCode() == Configuration.Code) {
                return true;
            }

            return false;
        }

        /// <inheritdoc />
        protected override void AssignFrom(Item item) { }

        /// <inheritdoc />
        public override bool PlacementTilePreview(AvatarController avatar, Entity entity, Universe universe, Vector3IMap<Tile> previews) {
            return false;
        }

        /// <inheritdoc />
        public override bool HasAssociatedToolComponent(Plukit.Base.Components components) {
            return false;
        }

        /// <inheritdoc />
        public override ItemRenderer FetchRenderer() {
            return _builder.Renderer;
        }

        /// <inheritdoc />
        public override bool CanBeGifted() {
            return false;
        }

        /// <inheritdoc />
        public override bool TryResolveMainInteractVerb(Entity entity, EntityUniverseFacade facade, Vector3I location,
            TileConfiguration lookedAtTile, out string verb) {
            verb = "nimbusfox.nimbustech.verb.placeLiquid";
            return true;
        }
    }
}
