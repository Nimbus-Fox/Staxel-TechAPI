﻿using System.Collections.Generic;
using NimbusFox.KitsuneCore;
using Plukit.Base;
using Staxel.Draw;

namespace NimbusFox.NimbusTech.Classes {
    public class MoldDrawable {
        private IReadOnlyList<MatrixDrawable> _drawables = new List<MatrixDrawable>();
        private float _percentage;
        private float _percentageStage;

        public void SetDrawables(IReadOnlyList<MatrixDrawable> drawables) {
            _drawables = drawables;

            _percentageStage = _drawables.Count == 0 ? 0 : 100f / _drawables.Count;
        }

        public void Update(LiquidContent content) {
            _percentage = content.PercentFull();
        }

        public void Render(DeviceContext context, ref Matrix4F matrix) {
            var index = 1;

            foreach (var drawable in _drawables) {
                if (_percentage > (index - 1) * _percentageStage && _percentage <= index * _percentageStage) {
                    var matr = matrix.Translate(0,
                        -(Helpers.GetPercentageOf(
                            100 - Helpers.CalculatePercentage(_percentage - ((index - 1) * _percentageStage),
                                _percentageStage), 0.05f)), 0);

                    drawable.Render(context, ref matr);
                }

                index++;
            }
        }
    }
}
