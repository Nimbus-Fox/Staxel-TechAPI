﻿using System;
using System.Collections.Generic;

namespace NimbusFox.NimbusTech.Classes {
    public class Cycle : IDisposable {
        private DateTime _lastCycle = DateTime.MinValue;
        private static bool _paused;
        private bool _wasPaused;

        private static List<Cycle> _cycles { get; } = new List<Cycle>();

        public Cycle() {
            _cycles.Add(this);
        }

        /// <summary>
        /// Runs function every 50 milliseconds equaling to 20 times a second
        /// whilst requiring to be called often. Pre, Post or the Update function is suggested.
        /// Will not run when game is paused
        /// </summary>
        /// <param name="function">Function to call if a cycle time has passed (50 milliseconds)</param>
        public void RunCycle(Action function) {
            if (!_paused) {
                var cyclesToRun = 0;

                if (_lastCycle == DateTime.MinValue) {
                    cyclesToRun = 1;
                } else {
                    var diff = new TimeSpan(DateTime.Now.Ticks - _lastCycle.Ticks);
                    if (diff.TotalMilliseconds >= 50) {
                        cyclesToRun = (int)Math.Floor(diff.TotalMilliseconds / 50);
                    }
                }

                for (var i = 0; i < cyclesToRun; i++) {
                    function();
                }

                if (cyclesToRun > 0) {
                    _lastCycle = DateTime.Now;
                }
            }
        }

        internal static void Pause() {
            if (!_paused) {
                _cycles.ForEach(x => x._lastCycle = DateTime.MinValue);
            }
            _paused = true;
        }

        internal static void Resume() {
            if (_paused) {
                _cycles.ForEach(x => x._lastCycle = DateTime.MinValue);
            }
            _paused = false;
        }

        /// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
        public void Dispose() {
            _cycles.Remove(this);
        }
    }
}
