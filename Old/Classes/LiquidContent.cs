﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.KitsuneCore.V1;
using NimbusFox.NimbusTech.Liquid;
using Staxel.Core;

namespace NimbusFox.NimbusTech.Classes {
    public class LiquidContent {
        public readonly LiquidConfiguration Liquid;
        private long _amount;
        public long Limit { get; private set; } = 0;
        private readonly Action _onChange;

        public LiquidContent(LiquidConfiguration config, Action onChange = null) {
            Liquid = config;
            _onChange = onChange;
            _amount = 0;
        }

        public bool Same(LiquidContent liquid) {
            return Liquid.Code == liquid.Liquid.Code;
        }

        public bool Same(string code) {
            return Liquid.Code == code;
        }

        public bool CanAdd(long amount) {
            if (amount < 1) {
                return false;
            }

            if (long.MaxValue - _amount < amount) {
                return false;
            }

            if (Limit > 0) {
                if (_amount + amount > Limit) {
                    return false;
                }
            }

            return true;
        }

        public bool TryAdd(long amount) {
            if (amount < 1) {
                return false;
            }

            if (long.MaxValue - _amount < amount) {
                return false;
            }

            if (Limit > 0) {
                if (_amount + amount > Limit) {
                    return false;
                }
            }

            _amount += amount;
            _onChange?.Invoke();

            return true;
        }

        public bool TryRemove(long amount) {
            if (amount < 1) {
                return false;
            }

            if (_amount - amount < 0) {
                return false;
            }

            _amount -= amount;
            _onChange?.Invoke();

            return true;
        }

        public long GetAmount() {
            return _amount;
        }

        public bool TrySetLimit(long value) {
            if (value < 0) {
                return false;
            }

            if (value < _amount) {
                return false;
            }

            Limit = value;
            return true;
        }

        public float PercentFull() {
            if (Limit == 0) {
                return 100;
            }

            return Helpers.CalculatePercentage(_amount, Limit);
        }
    }
}
