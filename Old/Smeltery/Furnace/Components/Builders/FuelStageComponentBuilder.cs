﻿using Plukit.Base;
using Staxel.Core;

namespace NimbusFox.NimbusTech.Smeltery.Furnace.Components.Builders {
    public class FuelStageComponentBuilder : IComponentBuilder {
        public string Kind() {
            return "fuelStages";
        }

        public object Instance(Blob config) {
            return new FuelStageComponent(config);
        }
    }
}
