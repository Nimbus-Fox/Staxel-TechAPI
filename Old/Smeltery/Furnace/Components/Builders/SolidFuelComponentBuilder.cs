﻿using Plukit.Base;
using Staxel.Core;

namespace NimbusFox.NimbusTech.Smeltery.Furnace.Components.Builders {
    public class SolidFuelComponentBuilder : IComponentBuilder{
        public string Kind() {
            return "solidFuel";
        }

        public object Instance(Blob config) {
            return new SolidFuelComponent(config);
        }
    }
}
