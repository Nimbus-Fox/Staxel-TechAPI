﻿using System.Collections.Generic;
using Plukit.Base;

namespace NimbusFox.NimbusTech.Smeltery.Furnace.Components {
    public class FuelStageComponent {
        public Dictionary<byte, string> Stages { get; }
        public FuelStageComponent(Blob config) {
            Stages = new Dictionary<byte, string>();

            foreach (var entry in config.KeyValueIteratable) {
                var key = entry.Key;
                if (key.Contains("%")) {
                    key = key.Replace("%", "");
                }

                if (byte.TryParse(key, out var result)) {
                    if (result <= 100) {
                        if (entry.Value.Kind == BlobEntryKind.String) {
                            Stages.Add(result, entry.Value.GetString());
                        }
                    }
                }
            }
        }
    }
}
