﻿using Plukit.Base;

namespace NimbusFox.NimbusTech.Smeltery.Furnace.Components {
    public class SolidFuelComponent {
        public long BurnTime { get; }

        public SolidFuelComponent(Blob config) {
            BurnTime = config.GetLong("burnTime");
        }
    }
}
