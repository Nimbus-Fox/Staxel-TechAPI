﻿using Plukit.Base;
using Staxel;
using Staxel.Client;
using Staxel.Core;
using Staxel.Draw;
using Staxel.Logic;
using Staxel.Rendering;

namespace NimbusFox.NimbusTech.Smeltery.Furnace.TileEntity {
    public class FurnaceTileEntityPainter : EntityPainter {

        private NameTag _nameTag;

        protected override void Dispose(bool disposing) {
            _nameTag.Dispose();
        }

        public override void RenderUpdate(Timestep timestep, Entity entity, AvatarController avatarController, EntityUniverseFacade facade,
            int updateSteps) { }

        public override void ClientUpdate(Timestep timestep, Entity entity, AvatarController avatarController, EntityUniverseFacade facade) { }
        public override void ClientPostUpdate(Timestep timestep, Entity entity, AvatarController avatarController, EntityUniverseFacade facade) { }

        public override void BeforeRender(DeviceContext graphics, Vector3D renderOrigin, Entity entity,
            AvatarController avatarController,
            Timestep renderTimestep) {
            if (entity.Logic is FurnaceTileEntityLogic furnaceLogic) {
                if (_nameTag == null) {
                    _nameTag = ClientContext.NameTagRenderer.RegisterNameTag(entity.Id);
                }

                _nameTag.Setup(furnaceLogic.Location.ToVector3D(), Constants.NameTagDefaultOffset, $"{furnaceLogic.FuelPercentage:N0}% | {furnaceLogic.DurationPercentage:N0}%", false, false, false, false);
            }
        }

        public override void Render(DeviceContext graphics, ref Matrix4F matrix, Vector3D renderOrigin, Entity entity,
            AvatarController avatarController, Timestep renderTimestep, RenderMode renderMode) {
        }

        public override void StartEmote(Entity entity, Timestep renderTimestep, EmoteConfiguration emote) { }
    }
}
