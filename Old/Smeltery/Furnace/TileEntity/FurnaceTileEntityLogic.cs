﻿using System.Collections.Generic;
using System.Linq;
using NimbusFox.KitsuneCore.V1;
using NimbusFox.NimbusTech.Classes;
using NimbusFox.NimbusTech.Smeltery.Furnace.TileState;
using Plukit.Base;
using Staxel.Core;
using Staxel.Crafting;
using Staxel.Items;
using Staxel.Logic;
using Staxel.TileStates.Docks;

namespace NimbusFox.NimbusTech.Smeltery.Furnace.TileEntity {
    public class FurnaceTileEntityLogic : EntityLogic {
        public Vector3I Location { get; private set; }
        private string TileCode { get; set; }
        protected Entity Entity { get; }
        public long FuelDuration { get; private set; }
        public long FuelDurationMax { get; private set; }
        public double FuelPercentage { get; private set; }
        public long CurrentDuration { get; private set; }
        public double DurationPercentage { get; private set; }
        protected bool NeedStore { get; private set; } = true;
        private Blob _constructionBlob;
        protected DockSite[] Inputs { get; private set; }
        protected DockSite[] Outputs { get; private set; }
        private ReactionDefinition[] _definitions;
        private Cycle Cycle { get; } = new Cycle();

        public FurnaceTileEntityLogic(Entity entity) {
            Entity = entity;
        }

        public override void PreUpdate(Timestep timestep, EntityUniverseFacade entityUniverseFacade) {
        }

        public override void Update(Timestep timestep, EntityUniverseFacade entityUniverseFacade) {
            if (Inputs != null && Outputs != null && _definitions != null) {
                Cycle.RunCycle(() => {
                    if (FuelDuration > 0) {
                        FuelDuration--;

                        if (FuelDuration <= 0) {
                            FuelDuration = 0;
                        }

                        var max = Helpers.CalculatePercentage(FuelDuration, FuelDurationMax);

                        if (max != FuelPercentage) {
                            FuelPercentage = max;
                            NeedsStore();
                        }

                        if (entityUniverseFacade.TryFetchTileStateEntityLogic(Location, TileAccessFlags.SynchronousWait, ChunkFetchKind.LivingWorld, EntityId.NullEntityId,
                            out var logic)) {
                            if (logic is FurnaceTileStateEntityLogic stateLogic) {
                                if (stateLogic.CanSmelt(Inputs, Outputs, _definitions, out var result)) {
                                    CurrentDuration++;

                                    max = Helpers.CalculatePercentage(CurrentDuration, result.CraftDuration ?? 0);

                                    if (max != DurationPercentage) {
                                        DurationPercentage = max;
                                        NeedsStore();
                                    }

                                    if (CurrentDuration == result.CraftDuration) {
                                        stateLogic.SmeltUpdate(Inputs, Outputs, result);
                                        CurrentDuration = 0;
                                    }
                                } else {
                                    CurrentDuration = 0;
                                    if (DurationPercentage != 0) {
                                        DurationPercentage = 0;
                                        NeedsStore();
                                    }

                                }
                            }
                        }
                    }
                });
            }
        }

        public override void PostUpdate(Timestep timestep, EntityUniverseFacade entityUniverseFacade) {
            if (Location == default(Vector3I)) {
                return;
            }
            if (entityUniverseFacade.ReadTile(Location, TileAccessFlags.SynchronousWait, ChunkFetchKind.LivingWorld, EntityId.NullEntityId, out var tile)) {
                if (TileCode != tile.Configuration.Code) {
                    entityUniverseFacade.RemoveEntity(Entity.Id);
                }
            }
        }

        public override void Construct(Blob arguments, EntityUniverseFacade entityUniverseFacade) {
            _constructionBlob = BlobAllocator.Blob(true);
            _constructionBlob.MergeFrom(arguments);
            Location = arguments.FetchBlob("location").GetVector3I();
            TileCode = arguments.GetString("tile");
            Entity.Physics.ForcedPosition(Location.ToVector3D());

            if (entityUniverseFacade.IsServer()) {
                var inputs = new List<DockSite>();
                var outputs = new List<DockSite>();

                if (entityUniverseFacade.TryFetchTileStateEntityLogic(Location, TileAccessFlags.SynchronousWait, ChunkFetchKind.LivingWorld, EntityId.NullEntityId,
                    out var logic)) {
                    if (logic is FurnaceTileStateEntityLogic stateLogic) {
                        foreach (var dockSite in stateLogic.GetDockSites()) {
                            if (dockSite.Config.SiteName.ToLower().StartsWith("input")) {
                                inputs.Add(dockSite);
                            }

                            if (dockSite.Config.SiteName.ToLower().StartsWith("output")) {
                                outputs.Add(dockSite);
                            }
                        }

                        Inputs = inputs.ToArray();
                        Outputs = outputs.ToArray();
                    }
                }

                _definitions = NimbusTechAPI.FurnaceSmeltReactions
                    .Where(x => x.Reagents.Count <= inputs.Count && x.Results.Count <= outputs.Count).ToArray();
            }
        }
        public override void Bind() { }
        public override bool Interactable() {
            return false;
        }

        public override void Interact(Entity entity, EntityUniverseFacade facade, ControlState main, ControlState alt) { }
        public override bool CanChangeActiveItem() {
            return false;
        }

        public override Heading Heading() {
            return new Heading();
        }

        public override bool IsPersistent() {
            return true;
        }

        public override void StorePersistenceData(Blob data) {
            data.SetLong("fuelDuration", FuelDuration);
            data.SetLong("fuelDurationMax", FuelDurationMax);
            data.SetLong("currentDuration", CurrentDuration);
            data.FetchBlob("construction").MergeFrom(_constructionBlob);
        }

        public override void RestoreFromPersistedData(Blob data, EntityUniverseFacade facade) {
            if (data.Contains("construction")) {
                Construct(data.FetchBlob("construction"), facade);
            }

            if (data.Contains("fuelDuration")) {
                FuelDuration = data.GetLong("fuelDuration");
            }

            if (data.Contains("fuelDurationMax")) {
                FuelDurationMax = data.GetLong("fuelDurationMax");
            }

            if (data.Contains("currentDuration")) {
                CurrentDuration = data.GetLong("currentDuration");
            }
        }

        public override void Store() {
            if (NeedStore) {
                StorePersistenceData(Entity.Blob);
                Entity.Blob.FetchBlob("location").SetVector3I(Location);
                Entity.Blob.SetDouble("fuelPercentage", FuelPercentage);
                Entity.Blob.SetDouble("durationPercentage", DurationPercentage);
                NeedStore = false;
            }
        }

        public override void Restore() {
            if (Entity.Blob.Contains("location")) {
                Location = Entity.Blob.FetchBlob("location").GetVector3I();
            }

            if (Entity.Blob.Contains("fuelPercentage")) {
                FuelPercentage = Entity.Blob.GetDouble("fuelPercentage");
            }

            if (Entity.Blob.Contains("durationPercentage")) {
                DurationPercentage = Entity.Blob.GetDouble("durationPercentage");
            }
        }

        public override bool IsCollidable() {
            return false;
        }

        protected void NeedsStore() {
            NeedStore = true;
        }

        public void SetFuelDuration(ulong duration) {
            if (duration > 0) {
                FuelDuration = (long) duration;
                FuelDurationMax = (long) duration;
                NeedsStore();
            }
        }
    }
}
