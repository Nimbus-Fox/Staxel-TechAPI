﻿using Plukit.Base;
using Staxel.Logic;
using Staxel.Tiles;

namespace NimbusFox.NimbusTech.Smeltery.Furnace.TileEntity {
    public class FurnaceTileEntityBuilder : IEntityLogicBuilder2, IEntityLogicBuilder, IEntityPainterBuilder {
        public static string KindCode => "nimbusfox.nimbustech.tileEntity.smeltingFurnace";
        public string Kind => KindCode;

        public EntityLogic Instance(Entity entity, bool server) {
            return new FurnaceTileEntityLogic(entity);
        }

        public EntityPainter Instance() {
            return new FurnaceTileEntityPainter();
        }

        public void Load() { }

        public bool IsTileStateEntityKind() {
            return false;
        }

        public static Entity Spawn(Vector3I position, EntityUniverseFacade universe, TileConfiguration tile) {
            var entity = new Entity(universe.AllocateNewEntityId(), false, KindCode, true);

            var blob = BlobAllocator.Blob(true);
            blob.SetString("kind", KindCode);
            blob.FetchBlob("position").SetVector3D(position.ToTileCenterVector3D());
            blob.FetchBlob("location").SetVector3I(position);
            blob.SetString("tile", tile.Code);

            entity.Construct(blob, universe);

            universe.AddEntity(entity);

            return entity;
        }
    }
}
