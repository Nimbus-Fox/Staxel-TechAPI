﻿using System.Collections.Generic;
using System.Linq;
using NimbusFox.KitsuneCore.V1;
using NimbusFox.NimbusTech.Smeltery.Furnace.Components;
using NimbusFox.NimbusTech.Smeltery.Furnace.TileEntity;
using Plukit.Base;
using Staxel.Crafting;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Player;
using Staxel.TileStates.Docks;

namespace NimbusFox.NimbusTech.Smeltery.Furnace.TileState {
    public class FurnaceTileStateEntityLogic : DockTileStateEntityLogic {
        public FurnaceTileStateEntityLogic(Entity entity) : base(entity) { }
        private EntityId _logicOwner = EntityId.NullEntityId;

        public override bool Interactable() {
            return true;
        }

        public override void Interact(Entity user, EntityUniverseFacade facade, ControlState main, ControlState alt) {
            if (user.Logic is PlayerEntityLogic == false) {
                return;
            }

            if (user.Inventory.ActiveItem().IsNull()) {
                base.Interact(user, facade, main, alt);
                return;
            }

            if (!user.Inventory.ActiveItem().Item.Configuration.Components.Contains<SolidFuelComponent>()) {
                base.Interact(user, facade, main, alt);
                return;
            }

            if (!main.DownClick) {
                base.Interact(user, facade, main, alt);
                return;
            }

            var item = user.Inventory.ActiveItem().Item;

            var comp = item.GetComponents().Get<SolidFuelComponent>();

            if (facade.TryGetEntity(_logicOwner, out var entity)) {
                if (entity.Logic is FurnaceTileEntityLogic furnaceLogic) {
                    if (furnaceLogic.FuelDuration == 0) {
                        furnaceLogic.SetFuelDuration((ulong)comp.BurnTime);
                        user.Inventory.RemoveItem(new ItemStack(item, 1));
                    } else {
                        base.Interact(user, facade, main, alt);
                    }
                } else {
                    base.Interact(user, facade, main, alt);
                }
            }
        }

        public override bool TryResolveMainInteractVerb(Entity entity, EntityUniverseFacade facade, ItemStack activeItem, out string verb) {
            verb = "";
            if (entity.Logic is PlayerEntityLogic == false) {
                return false;
            }

            if (!Equals(entity.Inventory.ActiveItem().Item, Item.NullItem)) {
                return false;
            }

            if (!entity.Inventory.ActiveItem().Item.Configuration.Components.Contains<SolidFuelComponent>()) {
                return false;
            }

            verb = activeItem.Item.InteractVerb();
            return true;
        }

        public override void PostUpdate(Timestep timestep, EntityUniverseFacade universe) {
            base.PostUpdate(timestep, universe);

            if (_logicOwner == EntityId.NullEntityId) {

                var entities = new Lyst<Entity>();

                universe.FindAllEntitiesInRange(entities, Location.ToVector3D(), 2F, entity => {
                    if (entity.Removed) {
                        return false;
                    }

                    if (entity.Logic is FurnaceTileEntityLogic) {
                        return true;
                    }

                    return false;
                });

                var tileEntity = entities.FirstOrDefault();

                if (tileEntity != default(Entity)) {
                    _logicOwner = tileEntity.Id;
                } else {
                    _logicOwner = FurnaceTileEntityBuilder.Spawn(Location, universe, _configuration).Id;
                }
            }
        }

        public void SmeltUpdate(DockSite[] inputs, DockSite[] outputs, ReactionDefinition result) {
            foreach (var input in inputs) {
                if (!input.IsEmpty()) {
                    var quantity = 0;
                    var set = false;
                    if (result.Reagents.Any(x => {
                        if (x.ItemBlob.GetString("code", x.ItemBlob.GetString("tile", "")) ==
                            input.DockedItem.Stack.Item.GetItemCode() &&
                            x.Quantity <= input.DockedItem.Stack.Count) {
                            if (!set) {
                                quantity = x.Quantity;
                                set = true;
                            }
                            return true;
                        }
                        return false;
                    })) {
                        input.CraftBasedUndock(new ItemStack(input.DockedItem.Stack.Item, quantity), out _);
                    }
                }
            }

            foreach (var item in result.Results) {
                foreach (var output in outputs) {
                    if (output.IsEmpty()) {
                        output.AddToDock(Entity, new ItemStack(Helpers.MakeItem(item.ItemBlob.GetString("code", item.ItemBlob.GetString("tile", ""))), item.Quantity));
                        break;
                    }

                    if (output.DockedItem.Stack.SingularItem().GetItemCode() ==
                        item.ItemBlob.GetString("code", item.ItemBlob.GetString("tile", ""))) {
                        output.AddToDock(Entity,
                            new ItemStack(Helpers.MakeItem(output.DockedItem.Stack.SingularItem().GetItemCode()),
                                item.Quantity));
                        break;
                    }
                }
            }
        }

        public bool CanSmelt(DockSite[] inputs, DockSite[] outputs, ReactionDefinition[] definitions, out ReactionDefinition result) {
            result = null;
            var smelt = false;
            if (inputs.All(x => x.IsEmpty())) {
                return false;
            }

            foreach (var definition in definitions) {
                if (definition.Reagents.Count > inputs.Count(x => !x.IsEmpty())) {
                    continue;
                }

                var validReagents = true;

                foreach (var reagent in definition.Reagents) {
                    var itemCode = reagent.ItemBlob.GetString("code", reagent.ItemBlob.GetString("tile", ""));
                    validReagents &= inputs.Any(x => x.DockedItem.Stack.Item.GetItemCode() == itemCode);

                    if (validReagents) {
                        validReagents &=
                            inputs.First(x => x.DockedItem.Stack.Item.GetItemCode() == itemCode).DockedItem.Stack
                                .Count >= reagent.Quantity;
                    } else {
                        break;
                    }
                }

                if (!validReagents) {
                    continue;
                }

                var canOutput = true;

                if (outputs.Count(x => x.IsEmpty()) < definition.Results.Count) {
                    if (outputs.Count(x => {
                        if (x.IsEmpty() || x.DockedItem.Stack.IsNull() || x.DockedItem.Stack.Count == 0) {
                            return true;
                        }

                        return definition.Results.Any(y =>
                            y.ItemBlob.GetString("code", y.ItemBlob.GetString("tile", "")) == x.DockedItem.Stack.Item.GetItemCode());
                    }) < definition.Results.Count) {
                        canOutput = false;
                    }
                }

                if (canOutput) {
                    smelt = true;
                    result = definition;
                    break;
                }
            }

            return smelt;
        }

        public IReadOnlyList<DockSite> GetDockSites() {
            return _dockSites;
        }

        public override string AltInteractVerb() {
            return "nimbusfox.nimbustech.verb.ignite";
        }

        public override bool SuppressInteractVerb() {
            return false;
        }
    }
}
