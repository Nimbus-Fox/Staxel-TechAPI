﻿using Plukit.Base;
using Staxel.Logic;
using Staxel.Tiles;
using Staxel.TileStates;

namespace NimbusFox.NimbusTech.Smeltery.Furnace.TileState {
    public class FurnaceTileStateBuilder : ITileStateBuilder {
        public void Dispose() { }
        public void Load() { }
        public string Kind() {
            return "nimbusfox.nimbustech.tileState.smeltingFurnace";
        }

        public Entity Instance(Vector3I location, Tile tile, Universe universe) {
            return FurnaceTileStateEntityBuilder.Spawn(universe, tile, location);
        }
    }
}
