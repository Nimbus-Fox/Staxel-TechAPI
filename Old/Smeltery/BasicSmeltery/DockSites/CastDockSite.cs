﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.NimbusTech.Classes;
using NimbusFox.NimbusTech.Interfaces;
using NimbusFox.NimbusTech.Liquid;
using Plukit.Base;
using Staxel;
using Staxel.Crafting;
using Staxel.Docks;
using Staxel.Logic;
using Staxel.Player;
using Staxel.TileStates.Docks;

namespace NimbusFox.NimbusTech.Smeltery.BasicSmeltery.DockSites {
    public class CastDockSite : DockSite, ILiquidContainer {
        private LiquidContent _content;
        private string _lastRecipeReagent = "";
        private int _lastAmount = 0;
        private List<ReactionDefinition> _validRecipes = new List<ReactionDefinition>();
        private ReactionDefinition _setRecipe;
        private bool _drying = false;
        /// <inheritdoc />
        public CastDockSite(Entity entity, DockSiteId id, DockSiteConfiguration dockSiteConfig) : base(entity, id, dockSiteConfig) { }

        /// <inheritdoc />
        public LiquidContent[] LiquidContents() {
            return new [] {_content};
        }

        /// <inheritdoc />
        public LiquidContent PrimaryLiquid() {
            return _content;
        }

        /// <inheritdoc />
        public bool TryGetLiquid(string code, out LiquidContent content) {
            content = null;

            if (_setRecipe != null) {
                var reagents = _setRecipe.Reagents.Where(x => x.ItemBlob.GetString("code", "") == code).ToArray();
                if (reagents.Length == 1) {
                    var first = reagents[0];

                    if (NimbusTechAPI.LiquidDatabase.TryGetLiquid(code, out var liquidConfig)) {
                        if (_content == null || _content?.GetAmount() == 0) {
                            _content = new LiquidContent(liquidConfig, NeedsStore);
                            _content.TrySetLimit(first.Quantity);
                        } else {
                            if (_content.Liquid.Code != code) {
                                return false;
                            }
                        }

                        content = _content;
                        return true;
                    }
                }
            }

            return false;
        }

        /// <inheritdoc />
        public bool CanAddByBucket(string liquid) {
            return false;
        }

        /// <inheritdoc />
        public bool CanRemoveByBucket() {
            return false;
        }

        /// <inheritdoc />
        public bool CanReceiveLiquids(params string[] liquids) {
            return _setRecipe != null && liquids.Any(code => _setRecipe.Reagents.Any(y => y.ItemBlob.GetString("code", "") == code));
        }

        /// <inheritdoc />
        public override bool TryUndock(PlayerEntityLogic player, EntityUniverseFacade facade, int quantity, int mult) {
            if (_content != null) {
                if (_content.GetAmount() > 0) {
                    return false;
                }
            }

            if (_drying) {
                return false;
            }

            return base.TryUndock(player, facade, quantity, mult);
        }

        /// <inheritdoc />
        public override void Update(Timestep timestep, EntityUniverseFacade universe) {
            if (DockedItem.Stack.IsNull() && !_lastRecipeReagent.IsNullOrEmpty()) {
                _lastRecipeReagent = "";
                _validRecipes.Clear();
                _setRecipe = null;
                _lastAmount = 0;
            }

            if (!DockedItem.Stack.IsNull()) {
                var stack = DockedItem.Stack;

                if (stack.Item.GetItemCode() != _lastRecipeReagent || _lastAmount != stack.Count) {
                    _lastRecipeReagent = stack.Item.GetItemCode();
                    _lastAmount = stack.Count;

                    _validRecipes = NimbusTechAPI.CastReactions.Where(x => x.Reagents.Any(y =>
                        stack.Item.GetItemCode() ==
                        GameContext.ItemDatabase.SpawnItem(y.ItemBlob, null).GetItemCode() &&
                        stack.Count >= y.Quantity)).ToList();

                    _setRecipe = null;
                }
            }


            base.Update(timestep, universe);
        }

        /// <inheritdoc />
        public override void Store(Blob blob) {
            if (_needsStore) {
                base.Store(blob);
                SharedStore(blob);
            }
        }

        /// <inheritdoc />
        public override void StorePersistenceData(Blob blob) {
            base.StorePersistenceData(blob);
            SharedStore(blob);
        }

        private void SharedStore(Blob blob) {
            if (_content != null) {
                if (_content.GetAmount() != 0) {
                    var liquidContents = blob.FetchBlob("liquidContents");

                    liquidContents.SetString("code", _content.Liquid.Code);
                    liquidContents.SetLong("limit", _content.Limit);
                    liquidContents.SetLong("amount", _content.GetAmount());
                }
            }

            if (!blob.Contains("liquidContents")) {
                var liquidContents = blob.FetchBlob("liquidContents");

                liquidContents.SetString("code", "empty");
            }

            blob.SetString("lastRecipeReagent", _lastRecipeReagent);
            blob.SetLong("lastAmount", _lastAmount);

            blob.SetString("recipe", _setRecipe == null ? "empty" : _setRecipe.Code);
        }

        /// <inheritdoc />
        public override void Restore(Blob blob) {
            base.Restore(blob);
            SharedRestore(blob);
        }

        /// <inheritdoc />
        public override void RestoreFromPersistedData(Blob blob) {
            base.RestoreFromPersistedData(blob);
            SharedRestore(blob);
        }

        private void SharedRestore(Blob blob) {
            if (blob.Contains("liquidContents")) {
                var liquidContents = blob.FetchBlob("liquidContents");

                if (liquidContents.GetString("code", "empty") == "empty") {
                    _content = null;
                } else {
                    if (NimbusTechAPI.LiquidDatabase.TryGetLiquid(liquidContents.GetString("code"), out var config)) {
                        _content = new LiquidContent(config, NeedsStore);
                        _content.TrySetLimit(liquidContents.GetLong("limit", 0));
                        _content.TryAdd(liquidContents.GetLong("amount", 0));
                    }
                }
            }

            if (blob.Contains("lastRecipeReagent")) {
                _lastRecipeReagent = blob.GetString("lastRecipeReagent");
            }

            if (blob.Contains("lastAmount")) {
                _lastAmount = (int)blob.GetLong("lastAmount");
            }

            if (blob.Contains("recipe")) {
                TrySetRecipe(blob.GetString("recipe"));
            }
        }

        private void NeedsStore() {
            _needsStore = true;
        }

        /// <inheritdoc />
        public override bool NeedsStorage() {
            NeedsStore();
            return base.NeedsStorage();
        }

        public bool TrySetRecipe(string code) {
            if (_validRecipes.Any(x => x.Code == code)) {
                _setRecipe = _validRecipes.First(x => x.Code == code);
                NeedsStore();
                return true;
            }

            return false;
        }

        public bool TryGetSetRecipe(out ReactionDefinition recipe) {
            recipe = null;
            if (_setRecipe == null) {
                return false;
            }

            recipe = _setRecipe;
            return true;
        }

        public IReadOnlyList<ReactionDefinition> GetValidRecipes() {
            return _validRecipes;
        }
    }
}
