﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.NimbusTech.Classes;
using NimbusFox.NimbusTech.Components;
using NimbusFox.NimbusTech.Interfaces;
using NimbusFox.NimbusTech.Liquid;
using NimbusFox.NimbusTech.Smeltery.BasicSmeltery.TileStateEntity;
using Plukit.Base;
using Staxel;
using Staxel.Core;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Player;

namespace NimbusFox.NimbusTech.Smeltery.BasicSmeltery.TileEntity {
    public class BasicSmelteryTileEntityLogic : EntityLogic, ILiquidContainer {
        protected Entity Entity;
        public Vector3I Location { get; private set; }
        protected bool NeedsStore { get; private set; }
        private Blob _constructor;
        private LiquidContent _liquidContents;
        public long FuelCycles { get; private set; }
        public long MaxFuelCycles { get; private set; }
        public long MeltingCycles { get; private set; }
        public long MaxMeltingCycles { get; private set; }
        private Cycle _cycle = new Cycle();
        public string TileCode { get; private set; }
        protected bool UpdateTheDisplay;
        private string _liquidCode = "";
        private long _amount;
        protected LiquidContainerComponent LiquidContainerComponent;

        public BasicSmelteryTileEntityLogic(Entity entity) {
            Entity = entity;
        }

        protected void NeedsStorage() {
            NeedsStore = true;
        }

        /// <inheritdoc />
        public override void PreUpdate(Timestep timestep, EntityUniverseFacade entityUniverseFacade) { }

        /// <inheritdoc />
        public override void Update(Timestep timestep, EntityUniverseFacade entityUniverseFacade) {
            _cycle.RunCycle(() => Cycle(entityUniverseFacade));
        }

        private void Cycle(EntityUniverseFacade entityUniverseFacade) {
            if (entityUniverseFacade.TryFetchTileStateEntityLogic(Location, TileAccessFlags.SynchronousWait,
                ChunkFetchKind.LivingWorld, Entity.Id, out var tileLogic)) {
                if (tileLogic is BasicSmelteryTileStateEntityLogic smelteryLogic) {
                    if ((MeltingCycles == MaxMeltingCycles && MaxMeltingCycles == 0) || FuelCycles == 0) {
                        if (FuelCycles == 0) {
                            var dock = smelteryLogic.GetFuelDock();
                            if (dock != null) {
                                if (!dock.DockedItem.Stack.IsNull()) {
                                    var item = dock.DockedItem.Stack.SingularItem();

                                    if (item.Configuration.Components.Contains<FuelComponent>()) {
                                        var component = item.Configuration.Components.Get<FuelComponent>();

                                        MaxFuelCycles = component.Cycles;
                                        FuelCycles = component.Cycles;

                                        var stack = dock.DockedItem.Stack;

                                        dock.EmptyWithoutExploding(entityUniverseFacade);

                                        stack.Count--;

                                        if (stack.Count > 0) {
                                            dock.AddToDock(Entity, stack);
                                        }

                                        NeedsStorage();
                                    }
                                }
                            }
                        }

                        if (FuelCycles != 0) {
                            var dock = smelteryLogic.GetMaterialDock();
                            if (dock != null) {
                                if (!dock.DockedItem.Stack.IsNull()) {
                                    var item = dock.DockedItem.Stack.SingularItem();

                                    if (item.Configuration.Components.Contains<SmeltComponent>()) {
                                        var component = item.Configuration.Components.Get<SmeltComponent>();

                                        if (TryGetLiquid(component.Liquid.Code, out var content)) {
                                            if (content.CanAdd(component.Amount)) {
                                                MaxMeltingCycles = component.Cycles;
                                                MeltingCycles = 0;

                                                _amount = component.Amount;
                                                _liquidCode = component.Liquid.Code;

                                                var stack = dock.DockedItem.Stack;

                                                dock.EmptyWithoutExploding(entityUniverseFacade);

                                                stack.Count--;

                                                if (stack.Count > 0) {
                                                    dock.AddToDock(Entity, stack);
                                                }

                                                NeedsStorage();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (MeltingCycles != MaxMeltingCycles && !_liquidCode.IsNullOrEmpty()) {
                        if (FuelCycles > 0) {
                            MeltingCycles++;
                            NeedsStorage();
                        } else {
                            if (MeltingCycles > 0) {
                                MeltingCycles--;
                                NeedsStorage();
                            }
                        }
                    } else {
                        if (TryGetLiquid(_liquidCode, out var contents)) {
                            contents.TryAdd(_amount);
                            _amount = 0;
                            MeltingCycles = 0;
                            MaxMeltingCycles = 0;
                            NeedsStorage();
                        }
                    }

                    if (FuelCycles != 0) {
                        FuelCycles--;
                        NeedsStorage();
                    }

                    if (TryGetLiquid(_liquidCode, out var liquidContents)) {
                        var containerDock = smelteryLogic.GetContainerCastDock();

                        if (containerDock != null) {
                            var recipes = containerDock.GetValidRecipes();

                            if (recipes.Count > 0) {
                                recipes = recipes.Where(x =>
                                    x.Reagents.Any(y =>
                                        y.ItemBlob.GetString("code", "") == _liquidCode && liquidContents.GetAmount() >=
                                        y.ItemBlob.GetLong("quantity", long.MaxValue))).ToList();

                                if (recipes.Count > 0) {
                                    containerDock.TrySetRecipe(recipes.First().Code);
                                }
                            }
                        }
                    }
                }

            }

        }

        /// <inheritdoc />
        public override void PostUpdate(Timestep timestep, EntityUniverseFacade entityUniverseFacade) {
            if (entityUniverseFacade.ReadTile(Location, TileAccessFlags.SynchronousWait, ChunkFetchKind.LivingWorld,
                Entity.Id, out var tile)) {
                if (tile.Configuration.Code != TileCode) {
                    entityUniverseFacade.RemoveEntity(Entity.Id);
                }
            }
        }

        /// <inheritdoc />
        public override void Construct(Blob arguments, EntityUniverseFacade entityUniverseFacade) {
            Location = arguments.GetBlob("location").GetVector3I();

            Entity.Physics.ForcedPosition(Location.ToVector3D());

            TileCode = arguments.GetString("tile", "");

            if (GameContext.TileDatabase.TryGetTileConfiguration(TileCode, out var config)) {
                if (config.Components.Contains<LiquidContainerComponent>()) {
                    LiquidContainerComponent = config.Components.Get<LiquidContainerComponent>();
                } else {
                    LiquidContainerComponent = new LiquidContainerComponent(arguments);
                }
            }

            _constructor = BlobAllocator.Blob(true);
            _constructor.MergeFrom(arguments);
            NeedsStorage();
        }

        /// <inheritdoc />
        public override void Bind() { }

        /// <inheritdoc />
        public override bool Interactable() {
            return false;
        }

        /// <inheritdoc />
        public override void Interact(Entity entity, EntityUniverseFacade facade, ControlState main, ControlState alt) { }

        /// <inheritdoc />
        public override bool CanChangeActiveItem() {
            return false;
        }

        /// <inheritdoc />
        public override Heading Heading() {
            return default;
        }

        /// <inheritdoc />
        public override bool IsPersistent() {
            return true;
        }

        /// <inheritdoc />
        public override void StorePersistenceData(Blob data) {
            data.FetchBlob("constructor").MergeFrom(_constructor);

            Store(data);
        }

        /// <inheritdoc />
        public override void RestoreFromPersistedData(Blob data, EntityUniverseFacade facade) {
            if (data.Contains("constructor")) {
                Construct(data.FetchBlob("constructor"), facade);
            }

            Restore(data);
        }

        /// <inheritdoc />
        public override void Store() {
            if (NeedsStore) {
                Store(Entity.Blob);
                NeedsStore = false;
            }
        }

        /// <inheritdoc />
        public override void Restore() {
            Restore(Entity.Blob);
        }

        private void Store(Blob data) {
            if (_liquidContents != null) {
                if (_liquidContents.GetAmount() != 0) {
                    var liquidContents = data.FetchBlob("liquidContents");

                    liquidContents.SetString("code", _liquidContents.Liquid.Code);
                    liquidContents.SetLong("limit", LiquidContainerComponent.Limit);
                    liquidContents.SetLong("amount", _liquidContents.GetAmount());
                }
            }

            if (!data.Contains("liquidContents")) {
                var liquidContents = data.FetchBlob("liquidContents");

                liquidContents.SetString("code", "empty");
            }

            var smeltery = data.FetchBlob("smeltery");

            smeltery.SetLong("fuelCycles", FuelCycles);
            smeltery.SetLong("maxFuelCycles", MaxFuelCycles);
            smeltery.SetLong("meltingCycles", MeltingCycles);
            smeltery.SetLong("maxMeltingCycles", MaxMeltingCycles);

            data.SetString("tileCode", TileCode);
        }

        private void Restore(Blob data) {
            if (data.Contains("liquidContents")) {
                var liquidContents = data.FetchBlob("liquidContents");

                if (liquidContents.GetString("code", "empty") == "empty") {
                    _liquidContents = null;
                } else {
                    if (NimbusTechAPI.LiquidDatabase.TryGetLiquid(liquidContents.GetString("code"), out var config)) {
                        _liquidContents = new LiquidContent(config, NeedsStorage);
                        _liquidContents.TrySetLimit(liquidContents.GetLong("limit", 0));
                        _liquidContents.TryAdd(liquidContents.GetLong("amount", 0));

                    }
                }
            }

            if (data.Contains("smeltery")) {
                var smeltery = data.FetchBlob("smeltery");

                if (smeltery.Contains("fuelCycles")) {
                    FuelCycles = smeltery.GetLong("fuelCycles");
                }

                if (smeltery.Contains("maxFuelCycles")) {
                    MaxFuelCycles = smeltery.GetLong("maxFuelCycles");
                }

                if (smeltery.Contains("meltingCycles")) {
                    MeltingCycles = smeltery.GetLong("meltingCycles");
                }

                if (smeltery.Contains("maxMeltingCycles")) {
                    MaxMeltingCycles = smeltery.GetLong("maxMeltingCycles");
                }
            }

            TileCode = data.GetString("tileCode", "");

            UpdateTheDisplay = true;
        }

        /// <inheritdoc />
        public override bool IsCollidable() {
            return false;
        }

        /// <inheritdoc />
        public LiquidContent[] LiquidContents() {
            return new[] { _liquidContents };
        }

        /// <inheritdoc />
        public LiquidContent PrimaryLiquid() {
            return _liquidContents;
        }

        /// <inheritdoc />
        public bool TryGetLiquid(string code, out LiquidContent content) {
            content = null;
            if (code.IsNullOrEmpty()) {
                return false;
            }

            if (_liquidContents == null || _liquidContents?.GetAmount() == 0) {
                if (NimbusTechAPI.LiquidDatabase.TryGetLiquid(code, out var config)) {
                    if (config.Components.Contains<LiquidMetalComponent>()) {
                        _liquidContents = new LiquidContent(config, NeedsStorage);
                        _liquidContents.TrySetLimit(LiquidContainerComponent.Limit);
                        content = _liquidContents;
                        return true;
                    }
                }
                return false;
            }

            if (_liquidContents.Same(code)) {
                content = _liquidContents;
                return true;
            }

            return false;
        }

        /// <inheritdoc />
        public bool CanAddByBucket(string liquid) {
            return true;
        }

        /// <inheritdoc />
        public bool CanRemoveByBucket() {
            return false;
        }

        /// <inheritdoc />
        public bool CanReceiveLiquids(params string[] liquids) {
            return true;
        }

        public bool UpdateDisplay() {
            if (!UpdateTheDisplay) {
                return false;
            }

            UpdateTheDisplay = false;
            return true;
        }
    }
}
