﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.NimbusTech.Classes;
using NimbusFox.NimbusTech.Mold.Components;
using Plukit.Base;
using Staxel;
using Staxel.Client;
using Staxel.Draw;
using Staxel.Logic;
using Staxel.Rendering;

namespace NimbusFox.NimbusTech.Smeltery.BasicSmeltery.TileEntity {
    public class BasicSmelteryTileEntityPainter : EntityPainter {
        private MoldDrawable _mold = new MoldDrawable();
        private LiquidContent _previous;
        private static readonly List<MatrixDrawable> _empty = new List<MatrixDrawable>();
        private Vector3F _offset = Vector3F.Zero;
        /// <inheritdoc />
        protected override void Dispose(bool disposing) { }

        /// <inheritdoc />
        public override void RenderUpdate(Timestep timestep, Entity entity, AvatarController avatarController, EntityUniverseFacade facade,
            int updateSteps) {
            if (entity.Logic is BasicSmelteryTileEntityLogic logic) {
                if (logic.PrimaryLiquid() != null) {
                    if (_previous == null || _previous?.Liquid.Code != logic.PrimaryLiquid().Liquid.Code) {
                        if (NimbusTechAPI.MoldDatabase.TryGetMolds(logic.PrimaryLiquid().Liquid, logic.TileCode,
                            out var drawables)) {
                            _mold.SetDrawables(drawables);
                            _previous = logic.PrimaryLiquid();
                        }
                    }

                    _mold.Update(logic.PrimaryLiquid());
                } else {
                    if (_previous != null) {
                        _previous = null;
                        _mold.SetDrawables(_empty);
                    }
                }

                if (!logic.TileCode.IsNullOrEmpty()) {
                    if (GameContext.TileDatabase.TryGetTileConfiguration(logic.TileCode, out var tileConfig)) {
                        if (tileConfig.Components.Contains<LiquidMoldsComponent>()) {
                            var component = tileConfig.Components.Get<LiquidMoldsComponent>();

                            _offset = component.Offset;
                        }
                    }
                }
            }
        }

        /// <inheritdoc />
        public override void ClientUpdate(Timestep timestep, Entity entity, AvatarController avatarController,
            EntityUniverseFacade facade) {
        }

        /// <inheritdoc />
        public override void ClientPostUpdate(Timestep timestep, Entity entity, AvatarController avatarController, EntityUniverseFacade facade) { }

        /// <inheritdoc />
        public override void BeforeRender(DeviceContext graphics, Vector3D renderOrigin, Entity entity, AvatarController avatarController,
            Timestep renderTimestep) { }

        /// <inheritdoc />
        public override void Render(DeviceContext graphics, ref Matrix4F matrix, Vector3D renderOrigin, Entity entity,
            AvatarController avatarController, Timestep renderTimestep, RenderMode renderMode) {

            var matr = matrix.Translate(entity.Physics.Position - renderOrigin).Translate(_offset);

            _mold.Render(graphics, ref matr);
        }

        /// <inheritdoc />
        public override void StartEmote(Entity entity, Timestep renderTimestep, EmoteConfiguration emote) { }
    }
}
