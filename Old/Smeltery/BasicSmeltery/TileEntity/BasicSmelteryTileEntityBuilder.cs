﻿using Plukit.Base;
using Staxel.Logic;
using Staxel.Tiles;

namespace NimbusFox.NimbusTech.Smeltery.BasicSmeltery.TileEntity {
    public class BasicSmelteryTileEntityBuilder : IEntityPainterBuilder, IEntityLogicBuilder2, IEntityLogicBuilder {
        public void Load() { }
        public string Kind => KindCode;
        public static string KindCode => "nimbusfox.nimbustech.entity.smeltery.basic";

        public bool IsTileStateEntityKind() {
            return false;
        }

        EntityLogic IEntityLogicBuilder.Instance(Entity entity, bool server) {
            return new BasicSmelteryTileEntityLogic(entity);
        }

        EntityPainter IEntityPainterBuilder.Instance() {
            return new BasicSmelteryTileEntityPainter();
        }

        public static Entity Spawn(Vector3I position, TileConfiguration tile, EntityUniverseFacade universe) {
            var entity = new Entity(universe.AllocateNewEntityId(), false, KindCode, true);

            var blob = BlobAllocator.Blob(true);
            blob.SetString("kind", KindCode);
            blob.FetchBlob("location").SetVector3I(position);
            blob.SetString("tile", tile.Code);

            entity.Construct(blob, universe);

            universe.AddEntity(entity);

            return entity;
        }
    }
}
