﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.NimbusTech.Smeltery.BasicSmeltery.DockSites;
using NimbusFox.NimbusTech.Smeltery.BasicSmeltery.TileEntity;
using Plukit.Base;
using Staxel.Logic;
using Staxel.TileStates.Docks;

namespace NimbusFox.NimbusTech.Smeltery.BasicSmeltery.TileStateEntity {
    public class BasicSmelteryTileStateEntityLogic : DockTileStateEntityLogic {

        public Entity ChildEntity { get; private set; }
        public EntityId ChildEntityId { get; private set; }
        private bool _needsStore;
        private bool _awaitingInit = true;

        /// <inheritdoc />
        public BasicSmelteryTileStateEntityLogic(Entity entity) : base(entity) { }

        /// <inheritdoc />
        public override void PostUpdate(Timestep timestep, EntityUniverseFacade universe) {
            base.PostUpdate(timestep, universe);

            if (_awaitingInit) {
                _awaitingInit = false;
                return;
            }

            if (ChildEntity == null || ChildEntity?.Removed == true) {
                var target = universe.FindNearestEntityInRange(Location.ToVector3D(), 1, entity => {
                    if (entity.IsDisposed) {
                        return false;
                    }

                    if (entity.Logic is BasicSmelteryTileEntityLogic logic) {
                        if (entity.Id == ChildEntityId || logic.Location == Location) {
                            return true;
                        }
                    }

                    return false;
                });

                if (target == null) {
                    ChildEntity = BasicSmelteryTileEntityBuilder.Spawn(Location, GetTileConfig(), universe);
                } else {
                    ChildEntity = target;
                }

                _needsStore = true;
            }
        }

        /// <inheritdoc />
        public override void StorePersistenceData(Blob blob) {
            blob.SetLong("childEntity", ChildEntity.Id.Id);
            base.StorePersistenceData(blob);
        }

        /// <inheritdoc />
        public override void RestoreFromPersistedData(Blob data, EntityUniverseFacade facade) {
            base.RestoreFromPersistedData(data, facade);

            if (data.Contains("childEntity")) {
                ChildEntityId = data.GetLong("childEntity");
            }
        }

        /// <inheritdoc />
        public override void Store() {
            base.Store();
            if (_needsStore) {
                Entity.Blob.SetLong("childEntity", ChildEntity.Id.Id);
                _needsStore = false;
            }
        }

        /// <inheritdoc />
        public override void Restore() {
            base.Restore();
            if (Entity.Blob.Contains("childEntity")) {
                ChildEntityId = Entity.Blob.GetLong("childEntity");
            }
        }

        public DockSite GetFuelDock() {
            return _dockSites.FirstOrDefault(x => x.Config.SiteName.ToLower().Contains("fuel"));
        }

        public DockSite GetMaterialDock() {
            return _dockSites.FirstOrDefault(x => x.Config.SiteName.ToLower().Contains("material"));
        }

        public DockSite GetOutputDock() {
            return _dockSites.FirstOrDefault(x => x.Config.SiteName.ToLower().Contains("output"));
        }

        public CastDockSite GetContainerCastDock() {
            var dock = _dockSites.FirstOrDefault(x => {
                var name = x.Config.SiteName.ToLower();

                return name.Contains("cast") || name.Contains("container");
            });

            if (dock is CastDockSite dockSite) {
                return dockSite;
            }

            return null;
        }

        /// <inheritdoc />
        public override bool Interactable() {
            return true;
        }

        /// <inheritdoc />
        public override void Interact(Entity user, EntityUniverseFacade facade, ControlState main, ControlState alt) {

            base.Interact(user, facade, main, alt);
        }

        /// <inheritdoc />
        public override bool TryResolveMainInteractVerb(Entity entity, EntityUniverseFacade facade, ItemStack activeItem, out string verb) {
            var parentResult = base.TryResolveMainInteractVerb(entity, facade, activeItem, out verb);

            if (parentResult) {
                return true;
            }

            verb = "";

            var containerSite = GetContainerCastDock();

            if (containerSite == null) {
                return false;
            }

            if (containerSite.TryGetSetRecipe(out _)) {
                verb = "nimbusfox.nimbustech.verb.pour";
                return true;
            }

            return false;
        }
    }
}
