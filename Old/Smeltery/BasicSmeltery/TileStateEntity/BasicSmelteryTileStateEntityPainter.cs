﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using NimbusFox.KitsuneCore;
using NimbusFox.KitsuneCore.V2;
using NimbusFox.KitsuneCore.V2.Classes;
using NimbusFox.KitsuneCore.V2.UI;
using NimbusFox.KitsuneCore.V2.UI.Elements;
using NimbusFox.NimbusTech.Smeltery.BasicSmeltery.TileEntity;
using Plukit.Base;
using Staxel;
using Staxel.Client;
using Staxel.Draw;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Rendering;
using Staxel.TileStates.Docks;

namespace NimbusFox.NimbusTech.Smeltery.BasicSmeltery.TileStateEntity {
    public class BasicSmelteryTileStateEntityPainter : DockTileStateEntityPainter {
        private readonly TextureRectangleDrawable _progressDrawable;
        private readonly TextureRectangleDrawable _filledDrawable;
        private float _fuelPercentage;
        private float _meltPercentage;
        private long _liquidContent;
        private long _liquidLimit;
        private long _lastContent;
        private long _lastLimit;

        /// <inheritdoc />
        public BasicSmelteryTileStateEntityPainter(DockTileStateEntityBuilder builder) : base(builder) {
            var config = BlobAllocator.Blob(true);
            config.SetString("code", "nimbusfox.nimbustech.item.coal");
            //_itemDrawable = GameContext.ItemDatabase.SpawnItem(config, Item.NullItem).Configuration.Icon;
            _progressDrawable = new TextureRectangleDrawable(new Vector2(100, 30), new UIPicture(context => {
                var canvas = new Texture2D(context.Graphics.GraphicsDevice, 150, 125, false, SurfaceFormat.Color);
                canvas.Clear(Color.Transparent);

                canvas.Paint(new Vector2I(130, 25), new Color(0, 0, 0, 100), new Vector2I(25, 25));
                canvas.Paint(new Vector2I((int)Math.Floor(Helpers.GetPercentageOf(_fuelPercentage, 130)), 25), Color.Orange, new Vector2I(25, 25));

                canvas.Paint(new Vector2I(130, 25), new Color(0, 0, 0, 100), new Vector2I(25, 75));
                canvas.Paint(new Vector2I((int)Math.Floor(Helpers.GetPercentageOf(_meltPercentage, 130)), 25), Color.Green, new Vector2I(25, 75));

                return canvas;
            }));

            _filledDrawable = new TextureRectangleDrawable(new Vector2(50, 15), new UIPicture(context => {
                if (UIManager.TryFetchFont(Constants.Fonts.MyFirstCrush, out var fontService, 100)) {
                    return fontService.RenderString($"{_liquidContent}/{_liquidLimit}").ToTexture2D(context);
                }
                return null;
            }));

            Blob.Deallocate(ref config);
        }

        /// <inheritdoc />
        public override void ClientPostUpdate(Timestep timestep, Entity entity, AvatarController avatarController, EntityUniverseFacade facade) {
            if (entity.Logic is BasicSmelteryTileStateEntityLogic logic) {
                if (facade.TryGetEntity(logic.ChildEntityId, out var childEntity)) {
                    if (childEntity.Logic is BasicSmelteryTileEntityLogic tileLogic) {
                        if (tileLogic.UpdateDisplay()) {
                            _fuelPercentage = Helpers.CalculatePercentage(tileLogic.FuelCycles, tileLogic.MaxFuelCycles);
                            _meltPercentage =
                                Helpers.CalculatePercentage(tileLogic.MeltingCycles, tileLogic.MaxMeltingCycles);
                            _progressDrawable.Redraw();

                            if (tileLogic.PrimaryLiquid() != null) {
                                if (tileLogic.PrimaryLiquid().GetAmount() > 0 && tileLogic.PrimaryLiquid().Limit > 0) {
                                    _liquidContent = tileLogic.PrimaryLiquid().GetAmount();
                                    _liquidLimit = tileLogic.PrimaryLiquid().Limit;
                                } else {
                                    _liquidLimit = -1;
                                }
                            }

                            if (_lastContent != _liquidContent || _lastLimit != _liquidLimit) {
                                _filledDrawable.Redraw();

                                _lastContent = _liquidContent;
                                _lastLimit = _liquidLimit;
                            }
                        }
                    }
                }
            }

            base.ClientUpdate(timestep, entity, avatarController, facade);
        }

        /// <inheritdoc />
        public override void Render(DeviceContext graphics, ref Matrix4F matrix, Vector3D renderOrigin, Entity entity,
            AvatarController avatarController, Timestep renderTimestep, RenderMode renderMode) {

            var corePos = entity.Physics.Position - renderOrigin;
            var matrix4F = matrix.Rotate(avatarController.Heading().Y, Vector3F.UnitX)
                .Rotate(avatarController.Heading().X, Vector3F.UnitY).Translate(corePos + new Vector3D(0, 1, 0));
            _progressDrawable.Render(graphics, ref matrix4F);

            if (_liquidLimit != -1) {
                matrix4F = matrix4F.Translate(new Vector3D(0, 0.3, 0));
                _filledDrawable.Render(graphics, ref matrix4F);
            }

            base.Render(graphics, ref matrix, renderOrigin, entity, avatarController, renderTimestep, renderMode);
        }
    }
}
