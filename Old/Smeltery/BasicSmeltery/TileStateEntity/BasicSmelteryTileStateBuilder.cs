﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plukit.Base;
using Staxel.Logic;
using Staxel.Tiles;
using Staxel.TileStates;
using Staxel.TileStates.Docks;

namespace NimbusFox.NimbusTech.Smeltery.BasicSmeltery.TileStateEntity {
    public class BasicSmelteryTileStateBuilder : ITileStateBuilder {
        /// <inheritdoc />
        public void Dispose() { }

        /// <inheritdoc />
        public void Load() { }

        /// <inheritdoc />
        public string Kind() {
            return KindCode;
        }

        public static string KindCode => "nimbusfox.nimbustech.tileState.smeltery.basic";

        /// <inheritdoc />
        public Entity Instance(Vector3I location, Tile tile, Universe universe) {
            return BasicSmelteryTileStateEntityBuilder.Spawn(universe, tile, location);
        }
    }
}
