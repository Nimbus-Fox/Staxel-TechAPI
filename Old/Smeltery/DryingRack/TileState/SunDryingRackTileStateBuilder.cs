﻿using Plukit.Base;
using Staxel.Logic;
using Staxel.Tiles;
using Staxel.TileStates;

namespace NimbusFox.NimbusTech.Smeltery.DryingRack.TileState {
    public class SunDryingRackTileStateBuilder : ITileStateBuilder {
        public void Dispose() { }
        public void Load() { }
        public string Kind() {
            return "nimbusfox.nimbustech.tileState.sunDryingRack";
        }

        public Entity Instance(Vector3I location, Tile tile, Universe universe) {
            return SunDryingRackTileStateEntityBuilder.Spawn(universe, tile, location);
        }
    }
}
