﻿using System.Linq;
using NimbusFox.KitsuneCore.V1;
using NimbusFox.NimbusTech.Smeltery.DryingRack.TileEntity;
using Plukit.Base;
using Staxel.Crafting;
using Staxel.Logic;
using Staxel.TileStates.Docks;

namespace NimbusFox.NimbusTech.Smeltery.DryingRack.TileState {
    class SunDryingRackTileStateEntityLogic : DockTileStateEntityLogic {

        private EntityId _logicOwner = EntityId.NullEntityId;

        public SunDryingRackTileStateEntityLogic(Entity entity) : base(entity) { }

        public override void PreUpdate(Timestep timestep, EntityUniverseFacade universe) {
            base.PreUpdate(timestep, universe);

            if (_logicOwner == EntityId.NullEntityId) {

                var entities = new Lyst<Entity>();

                universe.FindAllEntitiesInRange(entities, Location.ToVector3D(), 1F, entity => {
                    if (entity.Removed) {
                        return false;
                    }

                    if (entity.Logic is SunDryingRackTileEntityLogic) {
                        return true;
                    }

                    return false;
                });

                var tileEntity = entities.FirstOrDefault();

                if (tileEntity != default(Entity)) {
                    _logicOwner = tileEntity.Id;
                } else {
                    _logicOwner = SunDryingRackTileEntityBuilder.Spawn(Location, universe, _configuration).Id;
                }
            }
        }

        public override void Construct(Blob arguments, EntityUniverseFacade entityUniverseFacade) {
            base.Construct(arguments, entityUniverseFacade);
            Location = arguments.FetchBlob("location").GetVector3I();
        }

        public void RunCrafting(EntityUniverseFacade universe) {
            foreach (var dock in _dockSites) {
                if (!dock.IsEmpty()) {
                    var targetResult = NimbusTechAPI.SunDryReactions.FirstOrDefault(x =>
                        x.Reagents.Any(y => y.ItemBlob.GetString("code") == dock.DockedItem.Stack.Item.GetItemCode()));

                    if (targetResult != default(ReactionDefinition)) {
                        var toDock = new ItemStack(
                            Helpers.MakeItem(targetResult.Results.First().ItemBlob.GetString("code")),
                            dock.DockedItem.Stack.Count * targetResult.Results.First().Quantity);
                        dock.EmptyWithoutExploding(universe);
                        dock.AddToDock(null, toDock);
                    }
                }
            }
        }
    }
}
