﻿using NimbusFox.NimbusTech.Smeltery.DryingRack.TileState;
using Plukit.Base;
using Staxel.Core;
using Staxel.Items;
using Staxel.Logic;

namespace NimbusFox.NimbusTech.Smeltery.DryingRack.TileEntity {
    public class SunDryingRackTileEntityLogic : EntityLogic {
        private Vector3I Location { get; set; }
        private string TileCode { get; set; }
        private Entity _entity { get; }
        private bool Run { get; set; } = true;

        public SunDryingRackTileEntityLogic(Entity entity) {
            _entity = entity;
        }

        public override void PreUpdate(Timestep timestep, EntityUniverseFacade entityUniverseFacade) { }

        public override void Update(Timestep timestep, EntityUniverseFacade entityUniverseFacade) {
            if (entityUniverseFacade.DayNightCycle().Phase <= 0.3 && Run) {
                if (entityUniverseFacade.TryFetchTileStateEntityLogic(Location, TileAccessFlags.SynchronousWait, ChunkFetchKind.LivingWorld, EntityId.NullEntityId,
                    out var logic)) {
                    if (logic is SunDryingRackTileStateEntityLogic dryLogic) {
                        dryLogic.RunCrafting(entityUniverseFacade);
                    }
                }
                Run = false;
            }

            if (entityUniverseFacade.DayNightCycle().Phase >= 0.3 && !Run) {
                Run = true;
            }
        }

        public override void PostUpdate(Timestep timestep, EntityUniverseFacade entityUniverseFacade) {
            if (entityUniverseFacade.ReadTile(Location, TileAccessFlags.SynchronousWait, ChunkFetchKind.LivingWorld, EntityId.NullEntityId, out var tile)) {
                if (TileCode != tile.Configuration.Code) {
                    entityUniverseFacade.RemoveEntity(_entity.Id);
                }
            }
        }
        public override void Store() { }
        public override void Restore() { }

        public override void Construct(Blob arguments, EntityUniverseFacade entityUniverseFacade) {
            Location = arguments.FetchBlob("location").GetVector3I();
            TileCode = arguments.GetString("tile");
            _entity.Physics.ForcedPosition(Location.ToVector3D());
        }
        public override void Bind() { }
        public override bool Interactable() {
            return false;
        }

        public override void Interact(Entity entity, EntityUniverseFacade facade, ControlState main, ControlState alt) { }
        public override bool CanChangeActiveItem() {
            return false;
        }

        public override Heading Heading() {
            return new Heading();
        }

        public override bool IsPersistent() {
            return false;
        }

        public override void StorePersistenceData(Blob data) {
        }
        public override void RestoreFromPersistedData(Blob data, EntityUniverseFacade facade) { }
        public override bool IsCollidable() {
            return false;
        }
    }
}
