﻿using NimbusFox.NimbusTech.Classes;
using NimbusFox.NimbusTech.Liquid;

namespace NimbusFox.NimbusTech.Interfaces {
    public interface ILiquidContainer {
        LiquidContent[] LiquidContents();
        LiquidContent PrimaryLiquid();
        bool TryGetLiquid(string code, out LiquidContent content);
        bool CanAddByBucket(string liquid);
        bool CanRemoveByBucket();
        bool CanReceiveLiquids(params string[] liquids);
    }
}