﻿using System;
using System.Collections.Generic;
using System.Linq;
using NimbusFox.KitsuneCore.V1;
using NimbusFox.NimbusTech.Interfaces;
using NimbusFox.NimbusTech.Ore;
using Plukit.Base;
using Staxel;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Modding;
using Staxel.Tiles;

namespace NimbusFox.NimbusTech {
    internal class TechHook : IModHookV4 {
        internal static TechHook Instance;
        internal static OreHook OreInstance;

        internal readonly KitsuneCore.V1.KitsuneCore KsCore;

        public TechHook() {
            ModHelper.CheckModIsInstalled("NimbusTech", "Kitsune Core");
            Instance = this;
            KsCore = new KitsuneCore.V1.KitsuneCore("NimbusFox", "NimbusTech");
        }

        public void Dispose() { }
        public void GameContextInitializeInit() { }
        public void GameContextInitializeBefore() {
            NimbusTechAPI.LiquidComponentBuilders = Helpers.GetTypesUsingBase<ILiquidComponentBuilder>()
                .Select(instance => (ILiquidComponentBuilder) Activator.CreateInstance(instance))
                .ToDictionary(item => item.Kind());

            NimbusTechAPI.LiquidDatabase.Initialise();
        }

        public void GameContextInitializeAfter() {
            NimbusTechAPI.FurnaceSmeltReactions = GameContext.ReactionDatabase.GetAllReactionDefinitions().Values
                .Where(x => x.RecipeStep == "furnaceCook").ToList();

            NimbusTechAPI.SunDryReactions = GameContext.ReactionDatabase.GetAllReactionDefinitions().Values
                .Where(x => x.RecipeStep == "sunDry").ToList();

            NimbusTechAPI.CastReactions = GameContext.ReactionDatabase.GetAllReactionDefinitions().Values
                .Where(x => x.RecipeStep == "cast").ToList();

            NimbusTechAPI.LiquidDatabase.GenerateItems();

            if (!Helpers.IsServer()) {
                NimbusTechAPI.MoldDatabase.Initialise();
            }
        }

        public void GameContextDeinitialize() {
            NimbusTechAPI.LiquidDatabase.Deinitialise();

            if (!Helpers.IsServer()) {
                NimbusTechAPI.MoldDatabase.Deinitialise();
            }
        }
        public void GameContextReloadBefore() { }
        public void GameContextReloadAfter() { }
        public void UniverseUpdateBefore(Universe universe, Timestep step) { }
        public void UniverseUpdateAfter() { }
        public bool CanPlaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            return true;
        }

        public bool CanReplaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            return true;
        }

        public bool CanRemoveTile(Entity entity, Vector3I location, TileAccessFlags accessFlags) {
            return true;
        }

        public void ClientContextInitializeInit() { }
        public void ClientContextInitializeBefore() { }
        public void ClientContextInitializeAfter() { }
        public void ClientContextDeinitialize() { }
        public void ClientContextReloadBefore() { }
        public void ClientContextReloadAfter() { }
        public void CleanupOldSession() { }
        public bool CanInteractWithTile(Entity entity, Vector3F location, Tile tile) {
            return true;
        }

        public bool CanInteractWithEntity(Entity entity, Entity lookingAtEntity) {
            return true;
        }
    }
}
