﻿using System.Collections.Generic;
using NimbusFox.NimbusTech.Interfaces;
using NimbusFox.NimbusTech.Liquid;
using NimbusFox.NimbusTech.Mold;
using Staxel.Crafting;

public static class NimbusTechAPI {
    public static IReadOnlyList<ReactionDefinition> FurnaceSmeltReactions { get; internal set; }
    public static IReadOnlyList<ReactionDefinition> SunDryReactions { get; internal set; }
    public static IReadOnlyList<ReactionDefinition> CastReactions { get; internal set; }
    public static IReadOnlyDictionary<string, ILiquidComponentBuilder> LiquidComponentBuilders { get; internal set; }

    public static readonly LiquidDatabase LiquidDatabase = new LiquidDatabase();
    public static readonly MoldDatabase MoldDatabase = new MoldDatabase();
}
