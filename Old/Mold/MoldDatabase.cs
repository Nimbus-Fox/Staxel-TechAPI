﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.KitsuneCore.V1;
using NimbusFox.NimbusTech.Liquid;
using NimbusFox.NimbusTech.Mold.Components;
using Plukit.Base;
using Staxel;
using Staxel.Draw;
using Staxel.Items;
using Staxel.Tiles;
using Staxel.Voxel;

namespace NimbusFox.NimbusTech.Mold {
    public class MoldDatabase {
        private readonly Dictionary<string, Dictionary<string, List<MatrixDrawable>>> _renderings =
            new Dictionary<string, Dictionary<string, List<MatrixDrawable>>>();

        private readonly Dictionary<string, VoxelObject> _tempCache = new Dictionary<string, VoxelObject>();

        private static bool _first = true;

        public bool TryGetMolds(LiquidConfiguration liquid, string itemOrTile, out IReadOnlyList<MatrixDrawable> molds) {
            molds = new List<MatrixDrawable>();

            if (liquid == null || itemOrTile.IsNullOrEmpty()) {
                return false;
            }

            if (!_renderings.ContainsKey(itemOrTile)) {
                return false;
            }

            if (!_renderings[itemOrTile].ContainsKey(liquid.Code)) {
                return false;
            }

            molds = _renderings[itemOrTile][liquid.Code];
            return true;
        }

        internal void Initialise() {
            if (Helpers.IsContentBuilder() && _first) {
                _first = false;
                TechHook.Instance.KsCore.ConfigDirectory.DeleteDirectory("Mold Cache", true);
                while (TechHook.Instance.KsCore.ConfigDirectory.DirectoryExists("Mold Cache")) { }
            }

            var moldCache = TechHook.Instance.KsCore.ConfigDirectory.FetchDirectory("Mold Cache");
            var liquids = NimbusTechAPI.LiquidDatabase.GetConfigurations();
            var tiles = GameContext.TileDatabase.AllMaterials()
                .Where(x => x.Components.Contains<LiquidMoldsComponent>());

            foreach (var item in GameContext.ItemDatabase.SearchByComponent<LiquidMoldsComponent>()) {
                var currentCache = moldCache.FetchDirectory(item.Code);
                var moldComponent = item.Components.Get<LiquidMoldsComponent>();
                if (!_renderings.ContainsKey(item.Code)) {
                    _renderings.Add(item.Code, new Dictionary<string, List<MatrixDrawable>>());
                }

                foreach (var liquid in liquids) {
                    if (!_renderings[item.Code].ContainsKey(liquid.Code)) {
                        _renderings[item.Code].Add(liquid.Code, new List<MatrixDrawable>());
                    }

                    var index = 0;

                    foreach (var data in moldComponent.Molds) {
                        if (!currentCache.FileExists($"{liquid.Code}.{index}.qb")) {
                            if (!_tempCache.ContainsKey(data)) {
                                using (var ms = GameContext.ContentLoader.ReadStream(data)) {
                                    ms.Seek(0L, SeekOrigin.Begin);
                                    _tempCache.Add(data, VoxelLoader.LoadQb(ms, data, Vector3I.Zero, Vector3I.MaxValue));
                                }
                            }

                            var current = new VoxelObject(_tempCache[data].Dimensions,
                                _tempCache[data].Offset, _tempCache[data].Min,
                                _tempCache[data].Max, _tempCache[data].ColorData, true);

                            Helpers.VectorLoop(Vector3I.Zero, current.Dimensions - Vector3I.One, (x, y, z) => {
                                var col = current.Read(x, y, z);

                                if (col.A == 0) {
                                    return;
                                }

                                var transfer = liquid.FullModel.Read(x, y, z);

                                current.Write(x, y, z, transfer);
                            });

                            using (var fs = currentCache.ObtainFileStream($"{liquid.Code}.{index}.qb",
                                FileMode.Create)) {
                                current.SaveQB(fs);
                                fs.Flush(true);

                                fs.Close();
                            }

                            current.Dispose();
                        }

                        using (var ms = new MemoryStream()) {
                            using (var fs =
                                currentCache.ObtainFileStream($"{liquid.Code}.{index}.qb", FileMode.Open)) {
                                fs.Seek(0L, SeekOrigin.Begin);
                                fs.CopyTo(ms);
                            }

                            ms.Seek(0L, SeekOrigin.Begin);
                            var model = VoxelLoader.LoadQb(ms, $"{liquid.Code}.{index}.qb", Vector3I.Zero,
                                Vector3I.MaxValue);

                            var drawable = model.BuildVertices()
                                .Matrix(Matrix4F.Identity.Translate(new Vector3D(-0.5, 0, -0.5)));

                            _renderings[item.Code][liquid.Code].Add(drawable);
                        }

                        index++;
                    }
                }
            }

            foreach (var tile in tiles) {
                var currentCache = moldCache.FetchDirectory(tile.Code);
                var moldComponent = tile.Components.Get<LiquidMoldsComponent>();
                if (!_renderings.ContainsKey(tile.Code)) {
                    _renderings.Add(tile.Code, new Dictionary<string, List<MatrixDrawable>>());
                }

                foreach (var liquid in liquids) {
                    if (!_renderings[tile.Code].ContainsKey(liquid.Code)) {
                        _renderings[tile.Code].Add(liquid.Code, new List<MatrixDrawable>());
                    }

                    var index = 0;

                    foreach (var data in moldComponent.Molds) {
                        if (!currentCache.FileExists($"{liquid.Code}.{index}.qb")) {
                            if (!_tempCache.ContainsKey(data)) {
                                using (var ms = GameContext.ContentLoader.ReadStream(data)) {
                                    ms.Seek(0L, SeekOrigin.Begin);
                                    _tempCache.Add(data, VoxelLoader.LoadQb(ms, data, Vector3I.Zero, Vector3I.MaxValue));
                                }
                            }

                            var current = new VoxelObject(_tempCache[data].Dimensions,
                                _tempCache[data].Offset, _tempCache[data].Min,
                                _tempCache[data].Max, _tempCache[data].ColorData, true);

                            Helpers.VectorLoop(Vector3I.Zero, current.Dimensions - Vector3I.One, (x, y, z) => {
                                var col = current.Read(x, y, z);

                                if (col.A == 0) {
                                    return;
                                }

                                var transfer = liquid.FullModel.Read(x, y, z);

                                current.Write(x, y, z, transfer);
                            });

                            using (var fs = currentCache.ObtainFileStream($"{liquid.Code}.{index}.qb",
                                FileMode.Create)) {
                                current.SaveQB(fs);
                                fs.Flush(true);

                                fs.Close();
                            }

                            current.Dispose();
                        }

                        using (var ms = new MemoryStream()) {
                            using (var fs =
                                currentCache.ObtainFileStream($"{liquid.Code}.{index}.qb", FileMode.Open)) {
                                fs.Seek(0L, SeekOrigin.Begin);
                                fs.CopyTo(ms);
                            }

                            ms.Seek(0L, SeekOrigin.Begin);
                            var model = VoxelLoader.LoadQb(ms, $"{liquid.Code}.{index}.qb", Vector3I.Zero,
                                Vector3I.MaxValue);

                            var drawable = model.BuildVertices()
                                .Matrix(Matrix4F.Identity.Translate(new Vector3D(-0.5, 0, -0.5)));

                            _renderings[tile.Code][liquid.Code].Add(drawable);
                        }

                        index++;
                    }
                }
            }

            foreach (var temp in _tempCache) {
                temp.Value.Dispose();
            }

            _tempCache.Clear();
        }

        internal void Deinitialise() {
            foreach (var mold in _renderings) {
                foreach (var liquid in mold.Value) {
                    foreach (var val in liquid.Value) {
                        val.Dispose();
                    }
                }
            }

            _renderings.Clear();
        }
    }
}
