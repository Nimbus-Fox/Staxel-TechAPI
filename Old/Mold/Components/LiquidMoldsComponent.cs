﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plukit.Base;
using Staxel.Voxel;

namespace NimbusFox.NimbusTech.Mold.Components {
    public class LiquidMoldsComponent {
        public IReadOnlyList<string> Molds { get; }
        public Vector3F Offset { get; } = Vector3F.Zero;
        public LiquidMoldsComponent(Blob config) {
            var output = new List<string>();

            foreach (var mold in config.FetchList("molds")) {
                output.Add(mold.GetString());
            }

            Molds = output;

            if (config.Contains("offset")) {
                Offset = config.FetchBlob("offset").GetVector3F();
            }
        }
    }
}
