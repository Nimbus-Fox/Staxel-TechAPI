﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plukit.Base;
using Staxel.Core;

namespace NimbusFox.NimbusTech.Mold.Components.Builders {
    public class LiquidMoldsComponentBuilder : IComponentBuilder {
        /// <inheritdoc />
        public string Kind() {
            return "liquidMolds";
        }

        /// <inheritdoc />
        public object Instance(Blob config) {
            return new LiquidMoldsComponent(config);
        }
    }
}
