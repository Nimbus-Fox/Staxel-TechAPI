﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plukit.Base;

namespace NimbusFox.NimbusTech.Components {
    public class FuelComponent {
        public long Cycles { get; }
        public FuelComponent(Blob config) {
            Cycles = config.GetLong("cycles", 100);
        }
    }
}
