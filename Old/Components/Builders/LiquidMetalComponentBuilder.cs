﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.NimbusTech.Interfaces;
using Plukit.Base;
using Staxel.Core;

namespace NimbusFox.NimbusTech.Components.Builders {
    public class LiquidMetalComponentBuilder : ILiquidComponentBuilder {
        /// <inheritdoc />
        public string Kind() {
            return "liquidMetal";
        }

        /// <inheritdoc />
        public object Instance(Blob config) {
            return new LiquidMetalComponent(config);
        }
    }
}
