﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plukit.Base;
using Staxel.Items;

namespace NimbusFox.NimbusTech.Components.Builders {
    public class FuelComponentBuilder : IItemComponentBuilder {
        /// <inheritdoc />
        public string Kind() {
            return "fuel";
        }

        /// <inheritdoc />
        public object Instance(BaseItemConfiguration item, Blob config) {
            return new FuelComponent(config);
        }
    }
}
