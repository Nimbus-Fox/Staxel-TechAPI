﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plukit.Base;
using Staxel.Items;
using Staxel.Tiles;

namespace NimbusFox.NimbusTech.Components.Builders {
    public class LiquidContainerComponentBuilder : IItemComponentBuilder, ITileComponentBuilder {
        /// <inheritdoc />
        public object Instance(TileConfiguration tile, Blob config) {
            return new LiquidContainerComponent(config);
        }

        /// <inheritdoc />
        public object Instance(BaseItemConfiguration item, Blob config) {
            return new LiquidContainerComponent(config);
        }

        /// <inheritdoc />
        public string Kind() {
            return "liquidContainer";
        }
    }
}
