﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plukit.Base;
using Staxel.Items;
using Staxel.Tiles;

namespace NimbusFox.NimbusTech.Components.Builders {
    public class SmeltComponentBuilder : IItemComponentBuilder, ITileComponentBuilder {
        /// <inheritdoc />
        public string Kind() {
            return "smelt";
        }

        /// <inheritdoc />
        public object Instance(TileConfiguration tile, Blob config) {
            return new SmeltComponent(config);
        }

        /// <inheritdoc />
        public object Instance(BaseItemConfiguration item, Blob config) {
            return new SmeltComponent(config);
        }
    }
}
