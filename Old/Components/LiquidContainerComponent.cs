﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plukit.Base;

namespace NimbusFox.NimbusTech.Components {
    public class LiquidContainerComponent {
        public long Limit { get; }
        public LiquidContainerComponent(Blob config) {
            Limit = config.GetLong("limit", 4000);
        }
    }
}
