﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.NimbusTech.Liquid;
using Plukit.Base;

namespace NimbusFox.NimbusTech.Components {
    public class SmeltComponent {
        public long Cycles { get; }
        public LiquidConfiguration Liquid { get; }
        public long Amount { get; }

        public SmeltComponent(Blob config) {
            Cycles = config.GetLong("cycles", 600);
            Amount = config.GetLong("amount", 100);

            if (NimbusTechAPI.LiquidDatabase.TryGetLiquid(config.GetString("liquid"), out var liquidConfig)) {
                Liquid = liquidConfig;
            } else {
                throw new Exception("The Smelt component must have a valid liquid output");
            }
        }
    }
}
