﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plukit.Base;

namespace NimbusFox.NimbusTech.Components {
    public class LiquidMetalComponent {
        public long CyclesPer100mL { get; }
        public LiquidMetalComponent(Blob config) {
            CyclesPer100mL = config.GetLong("cyclesPer100mL", 100);
        }
    }
}
