﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plukit.Base;
using Staxel.Commands;
using Staxel.Entities;
using Staxel.Logic;
using Staxel.Server;

namespace NimbusFox.NimbusTech.Commands {
    public class SpawnLiquidCommand : ICommandBuilder {
        /// <inheritdoc />
        public string Execute(string[] bits, Blob blob, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            responseParams = new object[] { };

            if (NimbusTechAPI.LiquidDatabase.TryGetLiquid(bits[1], out var config)) {
                var item = config.MakeItem();

                var entity = TechHook.Instance.KsCore.UserManager.GetPlayerEntityByUid(connection.Credentials.Uid);

                entity.Inventory.GiveItem(new ItemStack(item, 1));

                return "nimbusfox.nimbustech.command.spawnLiquid.success";
            }

            return "nimbusfox.nimbustech.command.spawnLiquid.fail";
        }

        /// <inheritdoc />
        public string Kind => "spawnLiquid";

        /// <inheritdoc />
        public string Usage => "nimbusfox.nimbustech.command.spawnLiquid";

        /// <inheritdoc />
        public bool Public => false;
    }
}
