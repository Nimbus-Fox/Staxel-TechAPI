﻿using System;
using Plukit.Base;

namespace NimbusFox.NimbusTech.Transport.Components {
    public class RailComponent {
        public enum RailType {
            Straight,
            Corner,
            Cross
        }

        public double SpeedEfficiency { get; }
        public RailType Type { get; }

        public RailComponent(Blob config) {
            SpeedEfficiency = config.GetDouble("speedEfficiency", 0.8);
            if (Enum.TryParse<RailType>(config.GetString("type", "straight"), true, out var type)) {
                Type = type;
            } else {
                throw new Exception($"Rail type '{config.GetString("type", "straight")}' does not exist");
            }
        }
    }
}
