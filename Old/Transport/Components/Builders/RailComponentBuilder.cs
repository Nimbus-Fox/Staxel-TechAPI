﻿using Plukit.Base;
using Staxel.Core;

namespace NimbusFox.NimbusTech.Transport.Components.Builders {
    class RailComponentBuilder : IComponentBuilder {
        public string Kind() {
            return "rail";
        }

        public object Instance(Blob config) {
            return new RailComponent(config);
        }
    }
}
