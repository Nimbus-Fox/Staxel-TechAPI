﻿using System;
using NimbusFox.NimbusTech.Transport.Components;
using Plukit.Base;
using Staxel;
using Staxel.Logic;
using Staxel.Tiles;
using Staxel.TileStates;

namespace NimbusFox.NimbusTech.Transport.Minecart.Rail {
    public class RailTileStateEntityLogic : TileStateEntityLogic {
        public RailComponent Component { get; private set; }
        private uint _variant { get; set; }
        private TileConfiguration _tileConfig { get; set; }

        public RailTileStateEntityLogic(Entity entity) : base(entity) { }
        public override void PreUpdate(Timestep timestep, EntityUniverseFacade entityUniverseFacade) { }
        public override void Update(Timestep timestep, EntityUniverseFacade entityUniverseFacade) { }
        public override void PostUpdate(Timestep timestep, EntityUniverseFacade entityUniverseFacade) { }

        public override void Construct(Blob arguments, EntityUniverseFacade entityUniverseFacade) {
            _tileConfig = GameContext.TileDatabase.GetTileConfiguration(arguments.GetString("tile"));
            
            Component = _tileConfig.Components.Get<RailComponent>();

            _variant = Convert.ToUInt32(arguments.GetLong("variant"));
        }

        public override void Bind() { }

        public override bool Interactable() {
            return false;
        }

        public override void Interact(Entity entity, EntityUniverseFacade facade, ControlState main, ControlState alt) { }

        public override bool CanChangeActiveItem() {
            return false;
        }

        public override bool IsPersistent() {
            return false;
        }

        public override bool IsLingering() {
            return false;
        }

        public override void KeepAlive() { }

        public override void BeingLookedAt(Entity entity) { }

        public override bool IsBeingLookedAt() {
            return false;
        }

        public void NextRail(Vector3D currentLocation, out Vector3I targetLocation, out bool isRail) {
            targetLocation = Vector3I.Zero;
            isRail = false;
        }
    }
}
