﻿using Plukit.Base;
using Staxel.Logic;
using Staxel.Tiles;
using Staxel.TileStates;

namespace NimbusFox.NimbusTech.Transport.Minecart.Rail {
    public class RailTileStateEntityBuilder : IEntityPainterBuilder, IEntityLogicBuilder2, IEntityLogicBuilder {
        public static string KindCode => "nimbusfox.nimbustech.tileStateEntity.rail";
        public string Kind => KindCode;

        public EntityPainter Instance() {
            return new BasicTileStateEntityPainter();
        }

        public EntityLogic Instance(Entity entity, bool server) {
            return new RailTileStateEntityLogic(entity);
        }

        public void Load() { }

        public bool IsTileStateEntityKind() {
            return true;
        }

        public new static Entity Spawn(EntityUniverseFacade facade, Tile tile, Vector3I location) {
            var entity = new Entity(facade.AllocateNewEntityId(), false, KindCode, true);

            entity.Construct(GetBaseBlob(tile, location), facade);

            facade.AddEntity(entity);

            return entity;
        }

        public static Blob GetBaseBlob(Tile tile, Vector3I location) {
            var blob = BlobAllocator.Blob(true);
            blob.SetString("tile", tile.Configuration.Code);
            blob.SetLong("variant", tile.Variant());
            blob.FetchBlob("location").SetVector3I(location);
            blob.FetchBlob("velocity").SetVector3D(Vector3D.Zero);

            return blob;
        }
    }
}
