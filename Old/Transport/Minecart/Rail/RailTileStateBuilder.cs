﻿using Plukit.Base;
using Staxel.Logic;
using Staxel.Tiles;
using Staxel.TileStates;

namespace NimbusFox.NimbusTech.Transport.Minecart.Rail {
    public class RailTileStateBuilder : ITileStateBuilder {
        public void Dispose() { }
        public void Load() { }
        public string Kind() {
            return "nimbusfox.nimbustech.tileState.rail";
        }

        public Entity Instance(Vector3I location, Tile tile, Universe universe) {
            return RailTileStateEntityBuilder.Spawn(universe, tile, location);
        }
    }
}
