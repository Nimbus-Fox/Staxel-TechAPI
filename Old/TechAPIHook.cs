﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plukit.Base;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Modding;
using Staxel.Tiles;

namespace NimbusFox.TechAPI {
    public class TechAPIHook : IModHookV4 {

        public static TechAPIHook Instance;

        public TechAPIHook() {
            Instance = this;
        }

        /// <inheritdoc />
        public void Dispose() { }

        /// <inheritdoc />
        public void GameContextInitializeInit() { }

        /// <inheritdoc />
        public void GameContextInitializeBefore() { }

        /// <inheritdoc />
        public void GameContextInitializeAfter() { }

        /// <inheritdoc />
        public void GameContextDeinitialize() { }

        /// <inheritdoc />
        public void GameContextReloadBefore() { }

        /// <inheritdoc />
        public void GameContextReloadAfter() { }

        /// <inheritdoc />
        public void UniverseUpdateBefore(Universe universe, Timestep step) { }

        /// <inheritdoc />
        public void UniverseUpdateAfter() { }

        /// <inheritdoc />
        public bool CanPlaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            return true;
        }

        /// <inheritdoc />
        public bool CanReplaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            return true;
        }

        /// <inheritdoc />
        public bool CanRemoveTile(Entity entity, Vector3I location, TileAccessFlags accessFlags) {
            return true;
        }

        /// <inheritdoc />
        public void ClientContextInitializeInit() { }

        /// <inheritdoc />
        public void ClientContextInitializeBefore() { }

        /// <inheritdoc />
        public void ClientContextInitializeAfter() { }

        /// <inheritdoc />
        public void ClientContextDeinitialize() { }

        /// <inheritdoc />
        public void ClientContextReloadBefore() { }

        /// <inheritdoc />
        public void ClientContextReloadAfter() { }

        /// <inheritdoc />
        public void CleanupOldSession() { }

        /// <inheritdoc />
        public bool CanInteractWithTile(Entity entity, Vector3F location, Tile tile) {
            return false;
        }

        /// <inheritdoc />
        public bool CanInteractWithEntity(Entity entity, Entity lookingAtEntity) {
            return false;
        }
    }
}
