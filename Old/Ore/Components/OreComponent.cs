﻿using System.Collections.Generic;
using Plukit.Base;

namespace NimbusFox.NimbusTech.Ore.Components {
    public class OreComponent {
        public Dictionary<string, List<OreChance>> Config = new Dictionary<string, List<OreChance>>();
        
        public OreComponent(Blob config) {
            foreach (var entry in config.KeyValueIteratable) {
                var list = new List<OreChance>();

                foreach (var blob in entry.Value.List()) {
                    list.Add(new OreChance(blob.Blob()));
                }

                Config.Add(entry.Key, list);
            }
        }
    }
}
