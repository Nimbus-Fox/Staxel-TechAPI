﻿using Plukit.Base;
using Staxel.Core;

namespace NimbusFox.NimbusTech.Ore.Components.Builders {
    public class OreComponentBuilder : IComponentBuilder {
        public string Kind() {
            return "ore";
        }

        public object Instance(Blob config) {
            return new OreComponent(config);
        }
    }
}
