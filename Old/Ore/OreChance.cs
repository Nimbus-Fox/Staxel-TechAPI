﻿using System.IO;
using Plukit.Base;
using Staxel;

namespace NimbusFox.NimbusTech.Ore {
    public class OreChance {
        public double Chance { get; } = 0.5;
        public int Quantity { get; } = 1;
        public long MinimumHeight { get; } = 20;
        public long MaximumHeight { get; } = -20;

        public OreChance(Blob config) {
            if (config.Contains("__inherit")) {
                var inheritS = GameContext.ContentLoader.ReadStream(config.GetString("__inherit"));

                inheritS.Seek(0, SeekOrigin.Begin);

                var inherit = BlobAllocator.Blob(false);

                using (var sr = new StreamReader(inheritS)) {
                    inherit.ReadJson(sr.ReadToEnd());
                }

                Chance = inherit.GetDouble("chance", Chance);
                Quantity = (int)inherit.GetLong("quantity", Quantity);
                MinimumHeight = inherit.GetLong("minimumHeight", MinimumHeight);
                MaximumHeight = inherit.GetLong("maximumHeight", MaximumHeight);
            }

            Chance = config.GetDouble("chance", Chance);
            Quantity = (int)config.GetLong("quantity", Quantity);
            MinimumHeight = config.GetLong("minimumHeight", MinimumHeight);
            MinimumHeight = config.GetLong("minimumHeight", MinimumHeight);
            MaximumHeight = config.GetLong("maximumHeight", MaximumHeight);
        }
    }
}
