﻿using System.Collections.Generic;
using System.Linq;
using NimbusFox.KitsuneCore.V1;
using NimbusFox.NimbusTech.Ore.Components;
using Plukit.Base;
using Staxel;
using Staxel.Entities;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Modding;
using Staxel.Player;
using Staxel.Tiles;

namespace NimbusFox.NimbusTech.Ore {
    internal class OreHook : IModHookV4 {
        private Dictionary<string, Dictionary<string, List<OreChance>>> OreChances { get; } =
            new Dictionary<string, Dictionary<string, List<OreChance>>>();

        private int _next = 10;
        private byte _current;

        private bool _check = true;
        private Universe _universe;

        public OreHook() {
            TechHook.OreInstance = this;
        }

        public void Dispose() { }
        public void GameContextInitializeInit() { }
        public void GameContextInitializeBefore() { }

        public void GameContextInitializeAfter() {
            OreChances.Clear();

            foreach (var item in GameContext.ItemDatabase.SearchByComponent<OreComponent>()) {
                if (!OreChances.ContainsKey(item.Code)) {
                    OreChances.Add(item.Code, new Dictionary<string, List<OreChance>>());
                }

                var current = OreChances[item.Code];
                foreach (var data in item.Components.Get<OreComponent>().Config) {
                    if (!current.ContainsKey(data.Key)) {
                        current.Add(data.Key, data.Value);
                    } else {
                        current[data.Key].AddRange(data.Value);
                    }
                }
            }

            foreach (var tile in GameContext.TileDatabase.AllMaterials()
                .Where(x => x.Components.Contains<OreComponent>())) {

                if (!OreChances.ContainsKey(tile.Code)) {
                    OreChances.Add(tile.Code, new Dictionary<string, List<OreChance>>());
                }

                var current = OreChances[tile.Code];
                foreach (var data in tile.Components.Get<OreComponent>().Config) {
                    if (!current.ContainsKey(data.Key)) {
                        current.Add(data.Key, data.Value);
                    } else {
                        current[data.Key].AddRange(data.Value);
                    }
                }
            }
        }
        public void GameContextDeinitialize() { }
        public void GameContextReloadBefore() { }
        public void GameContextReloadAfter() { }

        public void UniverseUpdateBefore(Universe universe, Timestep step) {
            _universe = universe;
        }
        public void UniverseUpdateAfter() { }
        public bool CanPlaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            return true;
        }

        public bool CanReplaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            return true;
        }

        public bool CanRemoveTile(Entity entity, Vector3I location, TileAccessFlags accessFlags) {
            if (entity == null) {
                return true;
            }

            if (entity.Logic is PlayerEntityLogic playerLogic) {
                if (playerLogic.CreativeModeEnabled()) {
                    return true;
                }
            }
             
            if (_check) {
                _check = false;

                var allow = GameContext.ModdingController.CanRemoveTile(entity, location, accessFlags);
                if (!allow) {
                    return false;
                }

                _current++;

                if (_current != _next) {
                    _check = true;
                    return true;
                }

                _current = 0;
                _next = GameContext.RandomSource.Next(1, 20);

                if (_universe.ReadTile(location, accessFlags, ChunkFetchKind.LivingWorld, EntityId.NullEntityId, out var tile)) {
                    var output = new List<(string item, int quantity)>();

                    var chance = GameContext.RandomSource.NextDouble(0.0, 1.0);

                    foreach (var entry in OreChances) {
                        if (entry.Value.TryGetValue(tile.Configuration.Code, out var chances)) {
                            foreach (var oreChance in chances) {
                                if (location.Y <= oreChance.MinimumHeight && location.Y >= oreChance.MaximumHeight) {
                                    if (oreChance.Chance >= chance) {
                                        output.Add((entry.Key, oreChance.Quantity));
                                    }
                                }
                            }
                        }
                    }

                    if (output.Count > 0) {
                        var item = GameContext.RandomSource.Pick(output);
                        ItemEntityBuilder.SpawnDroppedItem(entity, _universe,
                            new ItemStack(Helpers.MakeItem(item.item), item.quantity), location.ToTileCenterVector3D(),
                            Vector3D.Zero, Vector3D.Zero, SpawnDroppedFlags.None);
                    }
                }
                _check = true;
            }
            return true;
        }

        public void ClientContextInitializeInit() { }
        public void ClientContextInitializeBefore() { }
        public void ClientContextInitializeAfter() { }
        public void ClientContextDeinitialize() { }
        public void ClientContextReloadBefore() { }
        public void ClientContextReloadAfter() { }
        public void CleanupOldSession() { }
        public bool CanInteractWithTile(Entity entity, Vector3F location, Tile tile) {
            return true;
        }

        public bool CanInteractWithEntity(Entity entity, Entity lookingAtEntity) {
            return true;
        }
    }
}
