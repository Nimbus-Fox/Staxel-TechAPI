﻿using System;
using NimbusFox.KitsuneToolBox.BlobDatabase.V1;
using Plukit.Base;

namespace NimbusFox.NimbusTech.Database {
    internal class CacheRecord : BaseRecord {
        public CacheRecord(BlobDatabase database, Blob blob, Guid id) : base(database, blob, id) { }

        public string File {
            get => _blob.GetString("file", "");
            set {
                _blob.SetString("file", value);
                Save();
            }
        }

        public string Hash {
            get => _blob.GetString("hash", "");
            set {
                _blob.SetString("hash", value);
                Save();
            }
        }
    }
}