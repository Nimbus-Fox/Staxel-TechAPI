﻿using System;
using System.Collections.Generic;
using System.Linq;
using NimbusFox.KitsuneToolBox.Extensions.V1;
using NimbusFox.NimbusTech.Context.Items;
using Plukit.Base;
using Staxel;

namespace NimbusFox.NimbusTech.Context {
    public class OreDatabase : IDisposable {
        private readonly Dictionary<string, Ore> _ores = new Dictionary<string, Ore>();

        internal OreDatabase() {
            foreach (var file in GameContext.AssetBundleManager.FindByExtension(".ore")) {
                using (var fs = GameContext.ContentLoader.ReadStream(file)) {
                    var blob = BlobAllocator.Blob(true);
                    blob.ReadJson(fs.ReadAllText());
                    var baseTemplate =
                        NimbusTechContext.BaseTemplateDatabase.FetchBaseTemplate(blob.GetString("baseTemplate"));
                    baseTemplate.MergeFrom(blob, true);
                    Blob.Deallocate(ref blob);
                    GameContext.Patches.PatchFile(file, baseTemplate);
                    baseTemplate.HandleInherits(true);
                    _ores.Add(baseTemplate.GetString("code"), new Ore(baseTemplate));
                    NimbusTechHook.Log($"Registering ore \"{baseTemplate.GetString("code")}\"");
                    Blob.Deallocate(ref baseTemplate);
                }
            }
        }

        public bool TryGetOre(string code, out Ore ore) {
            ore = null;
            if (_ores.ContainsKey(code)) {
                return false;
            }

            ore = _ores[code];
            return true;
        }

        public Ore[] GetAllOres() {
            return _ores.Values.ToArray();
        }

        public void Dispose() {
            _ores.Clear();
        }
    }
}