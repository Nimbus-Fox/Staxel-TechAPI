﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plukit.Base;
using Staxel;

namespace NimbusFox.NimbusTech.Context {
    public class BaseTemplateDatabase {
        private readonly Dictionary<string, Blob> _baseTemplates = new Dictionary<string, Blob>();
        internal BaseTemplateDatabase() {
        }

        internal void Initialize() {
            foreach (var blb in _baseTemplates.Values) {
                var b = blb;
                Blob.Deallocate(ref b);
            }

            _baseTemplates.Clear();
            foreach (var file in GameContext.AssetBundleManager.FindByExtension(".baseTemplate")) {
                using (var stream = GameContext.ContentLoader.ReadStream(file)) {
                    stream.Seek(0, SeekOrigin.Begin);
                    var blb = BlobAllocator.Blob(true);
                    blb.ReadJson(stream.ReadAllText());

                    GameContext.Patches.PatchFile(file, blb);

                    _baseTemplates.Add(file, blb);
                }
            }
        }

        public Blob FetchBaseTemplate(string file) {
            return _baseTemplates.ContainsKey(file)
                ? _baseTemplates[file].Clone()
                : BlobAllocator.Blob(true);
        }
    }
}
