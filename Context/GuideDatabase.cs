﻿using System;
using System.Collections.Generic;
using NimbusFox.NimbusTech.Context.Items;
using NimbusFox.NimbusTech.UI;

namespace NimbusFox.NimbusTech.Context {
    public class GuideDatabase : IDisposable {
        internal readonly Dictionary<string, List<GuideItem>> Entries = new Dictionary<string, List<GuideItem>>();

        //private Guide _guide;

        internal GuideDatabase() { }

        internal void BuildGui() {
            //_guide = new Guide();
        }

        public void RegisterItem(GuideItem item) {
//            if (!_entries.ContainsKey(item.Guide)) {
//                NimbusTechHook.Log($"Adding new guide \"{item.Guide}\"");
//                _entries.Add(item.Guide, new Dictionary<string, List<GuideItem>>());
//            }

            if (!Entries.ContainsKey(item.Chapter)) {
                NimbusTechHook.Log($"Adding chapter \"{item.Chapter}\"");
                Entries.Add(item.Chapter, new List<GuideItem>());
            }

            if (!Entries[item.Chapter].Contains(item)) {
                NimbusTechHook.Log($"Added \"{item.Code}\" to chapter \"{item.Chapter}\"");
                Entries[item.Chapter].Add(item);
            }
        }

        public void Dispose() {
            Entries.Clear();
            //_guide?.Dispose();
        }

        public void ShowGuide() {
            //_guide.ShowGuide();
        }

        public void HideGuide() {
            //_guide.HideGuide();
        }
    }
}