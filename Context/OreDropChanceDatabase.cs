﻿using System;
using System.Collections.Generic;
using NimbusFox.NimbusTech.Context.Items;
using Staxel;
using Staxel.Items;

namespace NimbusFox.NimbusTech.Context {
    public class OreDropChanceDatabase : IDisposable {
        private readonly Dictionary<string, Dictionary<int, List<(BaseItemConfiguration item, OreChance chance)>>>
            _chances = new Dictionary<string, Dictionary<int, List<(BaseItemConfiguration item, OreChance chance)>>>();

        private int _chanceOfOre = 0;

        internal OreDropChanceDatabase() { }

        public void AddDropChance(string tileCode, BaseItemConfiguration item, OreChance chance) {
            if (!_chances.ContainsKey(tileCode)) {
                _chances.Add(tileCode, new Dictionary<int, List<(BaseItemConfiguration item, OreChance chance)>>());
            }

            for (var y = chance.MinHeight; y >= chance.MaxHeight; y--) {
                if (!_chances[tileCode].ContainsKey(y)) {
                    _chances[tileCode].Add(y, new List<(BaseItemConfiguration item, OreChance chance)>());
                }

                _chances[tileCode][y].Add((item, chance));
            }
        }

        public Item GetRandomOre(string tileCode, int yLevel, out int quantity) {
            quantity = 0;
            if (!_chances.ContainsKey(tileCode)) {
                return Item.NullItem;
            }

            if (!_chances[tileCode].ContainsKey(yLevel)) {
                return Item.NullItem;
            }

            if (_chanceOfOre <= 0) {
                _chanceOfOre = GameContext.RandomSource.Next(0, (int)NimbusTechSettings.MaxStepsOreChance);
            }

            if (_chanceOfOre > 0) {
                _chanceOfOre--;
            }

            if (_chanceOfOre > 0) {
                return Item.NullItem;
            }

            var weights =
                new KeyValuePair<double, (BaseItemConfiguration item, OreChance chance)>[_chances[tileCode][yLevel]
                    .Count + 1];

            for (var i = 0; i < weights.Length - 1; i++) {
                weights[i] =
                    new KeyValuePair<double, (BaseItemConfiguration item, OreChance chance)>(
                        _chances[tileCode][yLevel][i].chance.Weight, _chances[tileCode][yLevel][i]);
            }

            weights[weights.Length - 1] = new KeyValuePair<double, (BaseItemConfiguration item, OreChance chance)>(300, (null, null));

            var chosen = GameContext.RandomSource.WeighedChoice(weights, weights.Length);

            if (chosen == (null, null)) {
                return Item.NullItem;
            }

            quantity = GameContext.RandomSource.Next(chosen.chance.MinQuantity, chosen.chance.MaxQuantity);

            return GameContext.ItemDatabase.InstanceFromItemConfiguration(chosen.item);
        }

        public void Dispose() {
            _chances.Clear();
        }
    }
}