﻿using System;
using System.Collections.Generic;
using System.Linq;
using NimbusFox.KitsuneToolBox.Extensions.V1;
using NimbusFox.NimbusTech.Context.Items;
using NimbusFox.NimbusTech.Enums;
using Plukit.Base;
using Staxel;

namespace NimbusFox.NimbusTech.Context {
    public class OreTemplateDatabase : IDisposable {
        private readonly Dictionary<string, OreTemplate> _oreTemplates = new Dictionary<string, OreTemplate>();
        
        internal OreTemplateDatabase() {
            foreach (var file in GameContext.AssetBundleManager.FindByExtension(".oreTemplate")) {
                using (var fs = GameContext.ContentLoader.ReadStream(file)) {
                    var blob = BlobAllocator.Blob(true);
                    blob.ReadJson(fs.ReadAllText());
                    blob.HandleInherits(true);
                    GameContext.Patches.PatchFile(file, blob);
                    blob.SetString("___resource", file);
                    _oreTemplates.Add(blob.GetString("code"), new OreTemplate(blob));
                    NimbusTechHook.Log($"Registering ore template \"{blob.GetString("code")}\"");
                    Blob.Deallocate(ref blob);
                }
            }
        }

        public bool TryGetOreTemplate(string code, out OreTemplate template) {
            template = null;
            if (!_oreTemplates.ContainsKey(code)) {
                return false;
            }

            template = _oreTemplates[code];
            return true;
        }

        public OreTemplate[] GetAllTemplates() {
            return _oreTemplates.Values.ToArray();
        }

        public OreTemplate[] GetAllTemplates(TemplateType templateType) {
            return _oreTemplates.Values.Where(x => templateType == x.TemplateType).ToArray();
        }

        public void Dispose() {
            _oreTemplates.Clear();
        }
    }
}