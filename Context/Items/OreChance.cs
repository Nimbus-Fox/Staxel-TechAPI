﻿using NimbusFox.KitsuneToolBox.Extensions.V1;
using Plukit.Base;

namespace NimbusFox.NimbusTech.Context.Items {
    public class OreChance {
        public readonly double Weight = 20;
        public readonly int MinQuantity = 1;
        public readonly int MaxQuantity = 1;
        public readonly int MinHeight = 20;
        public readonly int MaxHeight = -20;

        internal OreChance(Blob config) {
            config.HandleInherits(true);

            Weight = config.GetDouble("weight", Weight);

            var quantity = config.FetchBlob("quantity");
            MinQuantity = (int) quantity.GetLong("min", MinQuantity);
            MaxQuantity = (int) quantity.GetLong("max", MaxQuantity);

            var height = config.FetchBlob("height");
            MinHeight = (int) height.GetLong("min", MinHeight);
            MaxHeight = (int) height.GetLong("max", MaxHeight);
        }
    }
}