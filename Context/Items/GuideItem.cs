﻿using Plukit.Base;

namespace NimbusFox.NimbusTech.Context.Items {
    public class GuideItem {
        public readonly string Code;
        public readonly string Guide;
        public readonly string Chapter;
        public GuideItem(Blob config) {
            Code = config.GetString("code");
            Guide = config.GetString("guide", "");
            Chapter = config.GetString("chapter");
        }
    }
}