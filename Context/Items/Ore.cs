﻿using Microsoft.Xna.Framework;
using NimbusFox.KitsuneToolBox.Extensions.V1;
using Plukit.Base;

namespace NimbusFox.NimbusTech.Context.Items {
    public class Ore {
        public readonly string Code;
        internal readonly Blob Templates;
        internal readonly Blob GlobalConfig;
        public readonly Color Color;
        
        internal Ore(Blob blob) {
            Templates = BlobAllocator.Blob(true);
            Templates.AssignFrom(blob.FetchBlob("templates"));
            Templates.HandleCustomInherits(true);

            Code = blob.GetString("code");

            Color = !blob.Contains("color") ? Color.White : blob.KeyValueIteratable["color"].GetColor();
            
            GlobalConfig = BlobAllocator.Blob(true);
            GlobalConfig.AssignFrom(blob.FetchBlob("globalConfig"));
        }

        internal bool HasTemplate(string template, out Blob templateBlob) {
            templateBlob = null;
            if (!Templates.Contains(template)) {
                return true;
            }

            switch (Templates.KeyValueIteratable[template].Kind) {
                case BlobEntryKind.Bool:
                    return Templates.GetBool(template);
                case BlobEntryKind.Blob:
                    templateBlob = Templates.FetchBlob(template).Clone();
                    return true;
                default:
                    return false;
            }
        }
    }
}