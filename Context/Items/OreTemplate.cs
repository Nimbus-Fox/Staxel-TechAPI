﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Microsoft.Xna.Framework;
using NimbusFox.KitsuneToolBox.Extensions.V1;
using NimbusFox.KitsuneToolBox.Helpers.V1;
using NimbusFox.NimbusTech.Database;
using NimbusFox.NimbusTech.Enums;
using Plukit.Base;
using Staxel;
using Staxel.Core;
using Staxel.Crafting;
using Staxel.Draw;
using Staxel.Items;
using Staxel.Voxel;

namespace NimbusFox.NimbusTech.Context.Items {
    public class OreTemplate {
        public readonly string Code;
        public readonly TemplateType TemplateType;
        private readonly MemoryStream _model;
        private readonly string _modelResource;
        public readonly string CodePostFix;
        private readonly Blob _template;
        private readonly Color[] _colors;
        private readonly Dictionary<Color, Color> _changeableColors = new Dictionary<Color, Color>();
        private readonly float _colorRatio;

        internal OreTemplate(Blob blob) {
            Code = blob.GetString("code");

            switch (blob.GetString("type")) {
                case "tile":
                    TemplateType = TemplateType.Tile;
                    break;
                case "item":
                    TemplateType = TemplateType.Item;
                    break;
                case "gas":
                    TemplateType = TemplateType.Gas;
                    break;
                case "liquid":
                    TemplateType = TemplateType.Liquid;
                    break;
                case "recipe":
                    TemplateType = TemplateType.Recipe;
                    break;
                case "guide":
                    TemplateType = TemplateType.Guide;
                    break;
                case "reaction":
                    TemplateType = TemplateType.Reaction;
                    break;
                default:
                    throw new Exception("Invalid type given. Must be either: tile, item, gas, liquid, recipe, reaction or guide");
            }
            
            _template = BlobAllocator.Blob(true);
            _template.AssignFrom(blob.FetchBlob("template"));
            
            _modelResource = blob.GetString("baseModel", "");
            _template.SetString("icon", _modelResource);
            _template.SetString("inHand", _modelResource);

            if (!HelperFunctions.IsServer() && TemplateType != TemplateType.Guide && TemplateType != TemplateType.Recipe && TemplateType != TemplateType.Reaction) {
                _colorRatio = (float) blob.GetDouble("colorRatio", 0.5);
                
                var ms = new MemoryStream();
                using (var fs = GameContext.ContentLoader.ReadStream(blob.GetString("baseModel"))) {
                    fs.Seek(0L, SeekOrigin.Begin);
                    fs.CopyTo(ms);
                }

                ms.Seek(0L, SeekOrigin.Begin);
                _model = ms;

                if (blob.Contains("changeableColors")) {
                    using (var fs = GameContext.ContentLoader.ReadStream(blob.GetString("changeableColors"))) {
                        fs.Seek(0L, SeekOrigin.Begin);
                        using (var bitmap = BitmapLoader.LoadFromStream(fs)) {
                            for (var y = 0; y < bitmap.Height; y++) {
                                for (var x = 0; x < bitmap.Width; x++) {
                                    var current = bitmap.GetPixel(x, y);
                                    var col = new Color(current.R, current.G, current.B);
                                    if (!_changeableColors.ContainsKey(col)) {
                                        _changeableColors.Add(col, col);
                                    }
                                }
                            }
                        }

                        _colors = _changeableColors.Keys.ToArray();
                    }
                } else {
                    _colors = new Color[0];
                }
            }

            CodePostFix = blob.GetString("codePostfix");
        }

        internal void Generate(Ore ore) {
            if (ore.HasTemplate(Code, out var blob)) {
                if (blob == null) {
                    blob = BlobAllocator.Blob(true);
                    blob.AssignFrom(_template);
                } else {
                    if (TemplateType != TemplateType.Guide) {
                        var tmpBlob = BlobAllocator.Blob(true);
                        tmpBlob.AssignFrom(_template);
                        tmpBlob.MergeFrom(blob, true);
                        Blob.Deallocate(ref blob);
                        blob = tmpBlob;
                    }
                }

                blob.MergeFrom(ore.GlobalConfig.Clone(), true);

                blob.SetString("code", ore.Code + CodePostFix);

                switch (TemplateType) {
                    case TemplateType.Item:
                        GenerateItem(blob, ore.Color);
                        break;
                    case TemplateType.Guide:
                        GenerateGuide(blob);
                        break;
                    case TemplateType.Recipe:
                        GenerateRecipe(ore, blob);
                        break;
                    case TemplateType.Reaction:
                        GenerateReaction(ore, blob);
                        break;
                    default:
                        break;
                }
            }
        }

        private void GenerateItem(Blob blob, Color color) {
            NimbusTechHook.Log($"Generating item \"{blob.GetString("code")}\"");
            var itemConfig = new ItemConfiguration(blob, false, false);

            GameContext.CategoryDatabase.AddCategories(itemConfig.Categories);

            if (!HelperFunctions.IsServer() &&
                (blob.GetString("icon") == _modelResource || blob.GetString("inHand") == _modelResource)) {
                var existing =
                    (NimbusTechContext.CacheDatabase.SearchRecords<CacheRecord>(x =>
                        x.File == itemConfig.Code)).FirstOrDefault();

                var createCache = false;

                using (var fs = GameContext.ContentLoader.ReadStream(blob.GetString("icon"))) {
                    if (existing == default(CacheRecord)) {
                        var record = NimbusTechContext.CacheDatabase.CreateRecord((database, blob1, arg3) => new CacheRecord(database, blob1, arg3));
                        record.File = itemConfig.Code;
                        record.Hash = HelperFunctions.StringHash(fs);
                        createCache = true;
                    } else {
                        var hash = HelperFunctions.StringHash(fs);
                        if (existing.Hash != hash) {
                            existing.Hash = hash;
                            createCache = true;
                        }
                    }
                }

                var drawable = GenerateDrawable(createCache, color, itemConfig.Code);
                _model.Seek(0L, SeekOrigin.Begin);
                var voxels = VoxelLoader.LoadQb(_model, _modelResource, Vector3I.Zero, Vector3I.MaxValue);

                drawable = drawable
                    .Translate(new Vector3F(-voxels.Dimensions.X / 2f, -voxels.Min.Y, -voxels.Dimensions.Z / 2f) /
                               16f).CompressMatrix();

                voxels.Dispose();

                if (blob.GetString("icon") == _modelResource) {
                    itemConfig.Icon = drawable.Scale(itemConfig.IconScale).CompressMatrix();
                    itemConfig.CompactDrawable = drawable.Scale(Constants.CompactDrawableScalingFactor).CompressMatrix();
                }

                if (blob.GetString("inHand") == _modelResource) {
                    itemConfig.InHandDrawable = drawable.Scale(itemConfig.InHandScale).CompressMatrix();
                }
            }

            var itemConfigs =
                GameContext.ItemDatabase.GetPrivateFieldValue<Dictionary<string, ItemConfiguration>>(
                    "_itemConfigurations");

            itemConfigs.Add(itemConfig.Code, itemConfig);
        }

        private void GenerateGuide(Blob blob) {
            NimbusTechContext.GuideDatabase.RegisterItem(new GuideItem(blob));
        }

        private void GenerateRecipe(Ore ore, Blob blob) {
            NimbusTechHook.Log($"Generating recipe \"{blob.GetString("code")}\"");
            var recipeConfig = new RecipeConfiguration(blob, $"{blob.GetString("___resource")}:{blob.GetString("code")}", true);

            GameContext.CategoryDatabase.AddCategories(recipeConfig.Categories);

            var recipeConfigs =
                GameContext.RecipeDatabase.GetPrivateFieldValue<Dictionary<string, RecipeConfiguration>>(
                    "_recipeDefinitions");

            recipeConfigs.Add(recipeConfig.Code, recipeConfig);
        }

        private void GenerateReaction(Ore ore, Blob blob) {
            NimbusTechHook.Log($"Generating reaction \"{blob.GetString("code")}\"");

            var text = blob.ToString();

            var regex = new Regex("\\${.*?}");

            var results = regex.Matches(text);

            var oreType = typeof(Ore);

            foreach (Match result in results) {
                var bare = result.Value.Replace("${", "").Replace("}", "");

                if (!text.Contains(result.Value)) {
                    continue;
                }

                if (oreType.GetProperties().Any(x => x.Name == bare)) {
                    text = text.Replace(result.Value, ore.GetPrivatePropertyValue<object>(bare).ToString());
                    continue;
                }

                if (oreType.GetFields().Any(x => x.Name == bare)) {
                    text = text.Replace(result.Value, ore.GetPrivateFieldValue<object>(bare).ToString());
                }
            }

            var blob2 = BlobAllocator.Blob(true);

            blob2.ReadJson(text);

            var reactionConfig = new ReactionDefinition(blob2, true, $"Template: {Code} Ore:{ore.Code}");

            var reactionConfigs =
                GameContext.ReactionDatabase.GetPrivateFieldValue<Dictionary<string, ReactionDefinition>>(
                    "_reactionDefinitions");

            reactionConfigs.Add(reactionConfig.Code, reactionConfig);
        }

        private Drawable GenerateDrawable(bool createCache, Color color, string code) {
            if (createCache || !NimbusTechContext.QbCache.FileExists(code + ".cache")) {
                NimbusTechHook.Log($"Building model cache for {code}");
                _model.Seek(0L, SeekOrigin.Begin);
                var voxels = VoxelLoader.LoadQb(_model, _modelResource, Vector3I.Zero, Vector3I.MaxValue);

                foreach (var col in _colors) {
                    _changeableColors[col] = Color.Lerp(col, color, _colorRatio);
                }

                for (var i = 0; i < voxels.ColorData.Length; i++) {
                    if (_changeableColors.ContainsKey(voxels.ColorData[i])) {
                        voxels.ColorData[i] = _changeableColors[voxels.ColorData[i]];
                    }
                }

                using (var fs = NimbusTechContext.QbCache.ObtainFileStream(code + ".cache", FileMode.Create)) {
                    using (var ms = new MemoryStream()) {
                        voxels.BuildVertices().CompressedWriteToStream(ms, true);

                        ms.Seek(0L, SeekOrigin.Begin);
                        ms.CopyTo(fs);
                    }

                    fs.Flush(true);
                }

                voxels.Dispose();
            }

            using (var fs = NimbusTechContext.QbCache.ObtainFileStream(code + ".cache", FileMode.Open)) {
                using (var ms = new MemoryStream()) {
                    fs.Seek(0L, SeekOrigin.Begin);
                    fs.CopyTo(ms);

                    ms.Seek(0L, SeekOrigin.Begin);

                    var bytes = ms.ReadAllBytes();

                    return CompactVertexDrawable.InstanceFromCompressedStream(bytes, 0, bytes.Length);
                }
            }
        }
    }
}